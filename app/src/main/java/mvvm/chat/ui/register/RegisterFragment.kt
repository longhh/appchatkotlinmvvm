package mvvm.chat.ui.register

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import com.android.mvvm.R
import com.android.mvvm.databinding.FragmentRegisterBinding
import androidx.navigation.Navigation
import mvvm.chat.base.BaseFragment


class RegisterFragment : BaseFragment() {
    private lateinit var binding: FragmentRegisterBinding
    private lateinit var viewModel: RegisterViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentRegisterBinding.inflate(inflater, container, false).apply {
            viewModel =
                ViewModelProviders.of(this@RegisterFragment).get(RegisterViewModel::class.java)
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupListener()
        setupObserver()
        changeColorButton()
        view.setOnTouchListener { _, _ ->
            hideSoftKeyboard(requireActivity())
            false
        }
    }

    private fun setupListener() {
        binding.btnRegister.setOnClickListener {
            val username: String = binding.edtRegisterName.text.toString()
            val email: String = binding.edtRegisterEmail.text.toString()
            val pass: String = binding.edtRegisterPassword.text.toString()
            viewModel.register(username, email, pass)
        }

        binding.imgBack.setOnClickListener {
            Navigation.findNavController(requireView()).navigateUp()
        }

        binding.txtBack.setOnClickListener {
            Navigation.findNavController(requireView()).navigateUp()
        }
    }

    private fun setupObserver() {
        viewModel.resultData.observe(viewLifecycleOwner) {
            it?.let {
                if (it == 1) {
                    Toast.makeText(activity, R.string.register_success, Toast.LENGTH_SHORT).show()
                    NavHostFragment.findNavController(this)
                        .navigate(R.id.action_registerFragment_to_homeFragment, null)
                } else if (it == 2) {
                    Toast.makeText(activity, R.string.empty_details, Toast.LENGTH_SHORT).show()
                }
            }
        }

    }

    private fun changeColorButton() {
        binding.edtRegisterName.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable) {
                if (binding.edtRegisterName.text!!.isNotEmpty()
                    && binding.edtRegisterEmail.text!!.isNotEmpty()
                    && binding.edtRegisterPassword.text!!.isNotEmpty()
                    && binding.checkboxRegister.isChecked
                ) {
                    binding.btnRegister.setBackgroundResource(R.drawable.custom_button_blue)
                } else {
                    binding.btnRegister.setBackgroundResource(R.drawable.custom_button_grey)
                }
            }
        })
        binding.edtRegisterEmail.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable) {
                if (binding.edtRegisterName.text!!.isNotEmpty()
                    && binding.edtRegisterEmail.text!!.isNotEmpty()
                    && binding.edtRegisterPassword.text!!.isNotEmpty()
                    && binding.checkboxRegister.isChecked
                ) {
                    binding.btnRegister.setBackgroundResource(R.drawable.custom_button_blue)
                } else {
                    binding.btnRegister.setBackgroundResource(R.drawable.custom_button_grey)
                }
            }
        })
        binding.edtRegisterPassword.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable) {
                if (binding.edtRegisterName.text!!.isNotEmpty()
                    && binding.edtRegisterEmail.text!!.isNotEmpty()
                    && binding.edtRegisterPassword.text!!.isNotEmpty()
                    && binding.checkboxRegister.isChecked
                ) {
                    binding.btnRegister.setBackgroundResource(R.drawable.custom_button_blue)
                } else {
                    binding.btnRegister.setBackgroundResource(R.drawable.custom_button_grey)
                }
            }
        })
        binding.checkboxRegister.setOnCheckedChangeListener { _, _ ->
            if (binding.edtRegisterName.text!!.isNotEmpty()
                && binding.edtRegisterEmail.text!!.isNotEmpty()
                && binding.edtRegisterPassword.text!!.isNotEmpty()
                && binding.checkboxRegister.isChecked
            ) {
                binding.btnRegister.setBackgroundResource(R.drawable.custom_button_blue)
            } else {
                binding.btnRegister.setBackgroundResource(R.drawable.custom_button_grey)
            }
        }
    }
}