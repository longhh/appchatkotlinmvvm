package mvvm.chat.ui.register

import android.util.Log
import androidx.lifecycle.MutableLiveData
import mvvm.chat.base.BaseViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.ktx.Firebase
import mvvm.chat.utils.Constants
import java.util.*
import kotlin.collections.HashMap

class RegisterViewModel : BaseViewModel() {
    val resultData = MutableLiveData(0)
    fun register(username: String, email: String, pass: String) {
        val auth: FirebaseAuth = Firebase.auth
        auth.createUserWithEmailAndPassword(email, pass)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    assert(auth.currentUser != null)
                    val user = Firebase.auth.currentUser
                    user?.let {
                        val userid = user.uid
                        val hashMap = HashMap<String, String>()
                        hashMap[Constants.Firebase.FIRE_BASE_ID] = userid
                        hashMap[Constants.Firebase.FIRE_BASE_USERNAME] = username
                        hashMap[Constants.Firebase.FIRE_BASE_IMAGE_URL] = Constants.Firebase.FIRE_BASE_DEFAULT
                        hashMap[Constants.Firebase.FIRE_BASE_SEARCH] = username.toLowerCase(Locale.getDefault())
                        hashMap[Constants.Firebase.FIRE_BASE_PHONE_NUMBER] = Constants.Firebase.FIRE_BASE_DEFAULT
                        hashMap[Constants.Firebase.FIRE_BASE_DATE_OF_BIRTH] = Constants.Firebase.FIRE_BASE_DEFAULT
                        hashMap[Constants.Firebase.FIRE_BASE_ADDRESS] = Constants.Firebase.FIRE_BASE_DEFAULT
                        hashMap[Constants.Firebase.FIRE_BASE_EMAIL] = email

                        FirebaseDatabase.getInstance()
                            .getReference(Constants.Firebase.FIRE_BASE_USERS)
                            .child(userid)
                            .setValue(hashMap)
                            .addOnCompleteListener { task ->
                                if (task.isSuccessful) {
                                    login(email, pass)
                                } else {
                                    resultData.postValue(2)
                                }
                            }
                    }
                } else {
                    Log.w("abcd", "createUserWithEmail:failure", task.exception)
                }
            }
    }

    private fun login(email: String, pass: String){
        val auth: FirebaseAuth = Firebase.auth
        auth.signInWithEmailAndPassword(email, pass)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    resultData.postValue(1)
                } else {
                    resultData.postValue(2)
                }
            }
    }
}