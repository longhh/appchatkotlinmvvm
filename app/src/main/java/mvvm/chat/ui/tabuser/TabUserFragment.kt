package mvvm.chat.ui.tabuser

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import com.android.mvvm.R
import com.android.mvvm.databinding.FragmentTabUserBinding
import mvvm.chat.base.BaseFragment
import mvvm.chat.ui.homefriend.HomeFriendFragment
import mvvm.chat.utils.Constants
import mvvm.chat.utils.listener.OnItemClickListener

class TabUserFragment : BaseFragment() {

    private lateinit var viewModel: TabUserViewModel
    private lateinit var binding: FragmentTabUserBinding

    private var adapter: TabUserAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTabUserBinding.inflate(inflater, container, false).apply {
            viewModel =
                ViewModelProviders.of(this@TabUserFragment).get(TabUserViewModel::class.java)
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupObservers()
        setListeners()
        view.setOnTouchListener { _, _ ->
            hideSoftKeyboard(requireActivity())
            false
        }
    }

    private fun setListeners() {
        initAdapter()
        (parentFragment as HomeFriendFragment?)?.setOnTextChanged2(object :
            HomeFriendFragment.OnTextChanged2 {
            override fun onTextChanged2(word: String?) {
                if (word != null) {
                    viewModel.searchUser(word)
                }
            }
        })
    }

    private fun initAdapter() {
        adapter = context?.let { TabUserAdapter(it, ArrayList()) }
        binding.recyclerViewTabUser.adapter = adapter
    }

    private fun setupObservers() {
        viewModel.resultUserSort.observe(viewLifecycleOwner) {
            it?.let {
                if (adapter != null) {
                    adapter!!.setData(it)
                    adapter!!.setOnItemClickListener(object : OnItemClickListener {
                        override fun sendRequestFriend(position: Int) {
                            it[position].users?.let { it1 -> viewModel.sendRequest(it1, requireContext()) }
                        }

                        override fun cancelRequestFriend(position: Int) {
                            it[position].users?.let { it1 -> viewModel.cancelRequest(it1) }
                        }

                        override fun onItemClick(position: Int) {
                            val bundle = Bundle()
                            bundle.putSerializable(Constants.Bundle.BUNDLE_KEY_USERS, it[position].users)
                            NavHostFragment.findNavController(this@TabUserFragment)
                                .navigate(R.id.action_tabFriend_to_userProfileFragment, bundle)
                        }
                    })
                }
            }
        }
    }
}