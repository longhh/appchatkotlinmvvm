package mvvm.chat.ui.tabuser

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.mvvm.R
import com.android.mvvm.databinding.ItemTabUserBinding
import com.android.mvvm.databinding.ItemTabUserHeaderBinding
import com.bumptech.glide.Glide
import mvvm.chat.data.UserSort
import mvvm.chat.utils.Constants
import mvvm.chat.utils.listener.OnItemClickListener

class TabUserAdapter(private val context: Context, private var usersList: List<UserSort>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var onItemClickListener: OnItemClickListener? = null

    private val sectionView = 1
    private val contentView = 2

    @SuppressLint("NotifyDataSetChanged")
    fun setData(userList: List<UserSort>) {
        this.usersList = userList
        notifyDataSetChanged()
    }

    fun setOnItemClickListener(onItemClickListener: OnItemClickListener?) {
        this.onItemClickListener = onItemClickListener
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): RecyclerView.ViewHolder {
        return when (i) {
            sectionView -> {
                MyViewHolderHead(
                    ItemTabUserHeaderBinding.inflate(
                        LayoutInflater.from(viewGroup.context), viewGroup, false
                    )
                )
            }
            else -> {
                MyViewHolder(
                    ItemTabUserBinding.inflate(
                        LayoutInflater.from(viewGroup.context), viewGroup, false
                    )
                )
            }
        }
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, i: Int) {
        val userSort = usersList[i]
        when (viewHolder.itemViewType) {
            sectionView -> {
                val myViewHolderHead = viewHolder as MyViewHolderHead
                myViewHolderHead.bindView(userSort, context)
            }
            contentView -> {
                val myViewHolder = viewHolder as MyViewHolder
                myViewHolder.bindView(userSort, onItemClickListener, context)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (usersList[position].isSection) {
            sectionView
        } else {
            contentView
        }
    }

    override fun getItemCount(): Int {
        return usersList.size
    }

    class MyViewHolder(val binding: ItemTabUserBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bindView(user: UserSort, onItemClickListener: OnItemClickListener?, context: Context?) {
            when (user.stateUser) {
                Constants.StateUser.FRIEND -> {
                    binding.btnCancelRequestFr.visibility = View.GONE
                    binding.btnSendRequestFr.visibility = View.GONE
                }
                Constants.StateUser.REQUEST_FRIEND -> {
                    binding.btnCancelRequestFr.visibility = View.VISIBLE
                    binding.btnSendRequestFr.visibility = View.GONE
                }
                else -> {
                    binding.btnCancelRequestFr.visibility = View.GONE
                    binding.btnSendRequestFr.visibility = View.VISIBLE
                }
            }

            if (user.users?.imageURL.equals(Constants.Firebase.FIRE_BASE_DEFAULT)) {
                context?.let {
                    Glide.with(it).load(R.drawable.ic_avt).into(binding.imgCircleFriend)
                }
            } else {
                context?.let {
                    Glide.with(it).load(user.users?.imageURL).into(binding.imgCircleFriend)
                }
            }

            binding.txtUsername.text = user.users!!.username
            binding.itemTabUser.setOnClickListener {
                onItemClickListener?.onItemClick(adapterPosition)
            }

            binding.btnSendRequestFr.setOnClickListener {
                binding.btnCancelRequestFr.visibility = View.VISIBLE
                binding.btnSendRequestFr.visibility = View.GONE
                onItemClickListener?.sendRequestFriend(adapterPosition)
            }
            binding.btnCancelRequestFr.setOnClickListener {
                binding.btnCancelRequestFr.visibility = View.GONE
                binding.btnSendRequestFr.visibility = View.VISIBLE
                onItemClickListener?.cancelRequestFriend(adapterPosition)
            }
        }
    }

    class MyViewHolderHead(val binding: ItemTabUserHeaderBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindView(user: UserSort, context: Context?) {
            binding.txtHeader.text = user.header
        }
    }
}