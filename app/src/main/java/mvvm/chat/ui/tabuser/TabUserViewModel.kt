package mvvm.chat.ui.tabuser

import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.lifecycle.MutableLiveData
import com.android.mvvm.R
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.*
import com.google.firebase.ktx.Firebase
import mvvm.chat.base.BaseViewModel
import mvvm.chat.data.UserSort
import mvvm.chat.data.Users
import mvvm.chat.fcm.*
import mvvm.chat.model.api.APIFirebaseService
import mvvm.chat.utils.Constants
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class TabUserViewModel : BaseViewModel() {
    val resultUserSort = MutableLiveData<List<UserSort>>()

    var sender: Users? = null

    private var friends: ArrayList<Users> = ArrayList()
    private var friendRequestSends: ArrayList<Users> = ArrayList()
    private var users = ArrayList<Users>()
    private var userSorts: ArrayList<UserSort> = ArrayList()

    var apiService: APIFirebaseService = Client.getClient("https://fcm.googleapis.com/")!!.create(
        APIFirebaseService::class.java
    )

    init {
        getListFriend()
        getDataSender()
    }

    private fun getListFriend() {
        val userFirebase = Firebase.auth.currentUser
        userFirebase?.let {
            FirebaseDatabase.getInstance()
                .getReference(Constants.Reference.REFERENCE_FRIENDS)
                .child(it.uid)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        friends = ArrayList()
                        for (snapshot in dataSnapshot.children) {
                            val user = snapshot.getValue(Users::class.java)
                            if (user != null) {
                                friends.add(user)
                            }
                        }
                        getListFriendRequestSend()
                    }

                    override fun onCancelled(databaseError: DatabaseError) {
                    }
                })
        }
    }

    private fun getListFriendRequestSend() {
        val userFirebase = Firebase.auth.currentUser
        userFirebase?.let {
            FirebaseDatabase.getInstance()
                .getReference(Constants.Reference.REFERENCE_FRIEND_REQUEST)
                .child(it.uid)
                .child(Constants.Reference.REFERENCE_REQUEST_SEND)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        friendRequestSends = ArrayList()
                        for (snapshot in dataSnapshot.children) {
                            val user = snapshot.getValue(Users::class.java)
                            if (user != null) {
                                friendRequestSends.add(user)
                            }
                        }
                        getListUser()
                    }

                    override fun onCancelled(databaseError: DatabaseError) {}
                })
        }
    }

    private fun getListUser() {
        val userFirebase = Firebase.auth.currentUser
        userFirebase?.let {
            FirebaseDatabase.getInstance()
                .getReference(Constants.Reference.REFERENCE_USERS)
                .addValueEventListener(object : ValueEventListener {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        users = ArrayList()
                        for (snapshot in dataSnapshot.children) {
                            val user = snapshot.getValue(Users::class.java)
                            if (user != null && !user.id.equals(it.uid)) {
                                users.add(user)
                            }
                        }
                        sortListUserWithAlphabet()
                    }

                    override fun onCancelled(databaseError: DatabaseError) {
                    }
                })
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private fun sortListUserWithAlphabet() {
        val sortedUsers = users.sortedBy { it.username?.first()?.toUpperCase().toString() }
        users = ArrayList(sortedUsers)
        filterListUser()
    }

    private fun filterListUser() {
        userSorts = ArrayList()
        var lastHeader = ""
        for (user in users) {
            val header: String? =
                user.username?.let {
                    java.lang.String.valueOf(it.first())
                        .toUpperCase(Locale.getDefault())
                }
            val currentState: Constants.StateUser = getStateUser(user)
            if (header != lastHeader) {
                if (header != null) {
                    lastHeader = header
                }
                header?.let { UserSort(it, null, true, currentState) }?.let { userSorts.add(it) }
            }
            header?.let { UserSort(it, user, false, currentState) }?.let { userSorts.add(it) }
        }
        resultUserSort.postValue(userSorts)
    }

    private fun getStateUser(user: Users): Constants.StateUser {
        var state: Constants.StateUser = Constants.StateUser.OTHERS
        for (friend in friends) {
            if (user.id.equals(friend.id)) {
                state = Constants.StateUser.FRIEND
            }
        }
        for (friendRequest in friendRequestSends) {
            if (user.id.equals(friendRequest.id)) {
                state = Constants.StateUser.REQUEST_FRIEND
            }
        }
        return state
    }

    private fun getDataSender() {
        val user = Firebase.auth.currentUser
        user?.let {
            FirebaseDatabase.getInstance().getReference(Constants.Reference.REFERENCE_USERS)
                .child(it.uid)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        sender = dataSnapshot.getValue(Users::class.java)
                    }

                    override fun onCancelled(databaseError: DatabaseError) {
                    }
                })
        }
    }

    private fun removeValueSender(users: Users, id: String, table: String) {
        val query: Query = FirebaseDatabase.getInstance().reference
            .child(Constants.Reference.REFERENCE_FRIEND_REQUEST)
            .child(id)
            .child(table)
            .orderByChild(Constants.Firebase.FIRE_BASE_ID).equalTo(users.id)
        query.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (appleSnapshot in dataSnapshot.children) {
                    appleSnapshot.ref.removeValue()
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {}
        })
    }

    fun sendRequest(user: Users, context: Context) {
        val users = Firebase.auth.currentUser
        users?.let {
            FirebaseDatabase.getInstance().reference
                .child(Constants.Reference.REFERENCE_FRIEND_REQUEST)
                .child(it.uid)
                .child(Constants.Reference.REFERENCE_REQUEST_SEND)
                .push()
                .setValue(user)
            user.let { it1 ->
                FirebaseDatabase.getInstance().reference
                    .child(Constants.Reference.REFERENCE_FRIEND_REQUEST)
                    .child(it1.id.toString())
                    .child(Constants.Reference.REFERENCE_REQUEST_RECEIVE)
                    .push()
                    .setValue(sender)
                sendNotification(it1, it.uid, context)
            }
        }
    }

    private fun sendNotification(users: Users, uid: String, context: Context) {
        FirebaseDatabase.getInstance()
            .getReference(Constants.Reference.REFERENCE_TOKENS)
            .orderByKey()
            .equalTo(users.id)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    for (dataSnapshot in snapshot.children) {
                        val token = dataSnapshot.getValue(Token::class.java)
                        val data = Data(
                            uid,
                            R.mipmap.ic_launcher,
                            sender!!.username.toString() + " " + context.getString(R.string.sent_add_friend),
                            context.getString(R.string.text_noti),
                            users.id!!
                        )
                        assert(token != null)
                        val sender = Sender(data, token!!.token)
                        apiService.sendNotifications(sender)
                            ?.enqueue(object : Callback<MyResponse?> {
                                override fun onResponse(
                                    call: Call<MyResponse?>,
                                    response: Response<MyResponse?>
                                ) {
                                    if (response.code() == 200) {
                                        assert(response.body() != null)
                                    }
                                }

                                override fun onFailure(call: Call<MyResponse?>, t: Throwable) {}
                            })
                    }
                }

                override fun onCancelled(error: DatabaseError) {}
            })
    }

    fun cancelRequest(user: Users) {
        val users = Firebase.auth.currentUser
        users?.let {
            removeValueSender(
                user,
                it.uid,
                Constants.Reference.REFERENCE_REQUEST_SEND
            )
            user.id?.let { it1 ->
                removeValueSender(
                    sender!!,
                    it1, Constants.Reference.REFERENCE_REQUEST_RECEIVE
                )
            }
        }
    }

    fun searchUser(word: String) {
        val userFirebase = Firebase.auth.currentUser
        userFirebase?.let {
            FirebaseDatabase.getInstance()
                .getReference(Constants.Reference.REFERENCE_USERS)
                .orderByChild(Constants.Firebase.FIRE_BASE_SEARCH)
                .startAt(word).endAt(word + "\uf8ff")
                .addValueEventListener(object : ValueEventListener {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    override fun onDataChange(snapshot: DataSnapshot) {
                        users.clear()
                        for (dataSnapshot in snapshot.children) {
                            val user = dataSnapshot.getValue(Users::class.java)!!
                            if (!user.id.equals(it.uid)) {
                                users.add(user)
                            }
                        }
                        sortListUserWithAlphabet()
                    }

                    override fun onCancelled(error: DatabaseError) {}
                })
        }
    }
}