package mvvm.chat.ui.messenger

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.LayoutManager
import com.android.mvvm.R
import com.android.mvvm.databinding.DialogTransferMessageBinding
import com.android.mvvm.databinding.FragmentMessengerBinding
import com.bumptech.glide.Glide
import mvvm.chat.base.BaseFragment
import mvvm.chat.data.Message
import mvvm.chat.data.Room
import mvvm.chat.utils.Constants
import mvvm.chat.utils.listener.OnLongClickListener
import java.text.ParseException
import java.util.*
import kotlin.collections.ArrayList


class MessengerFragment : BaseFragment() {

    private lateinit var viewModel: MessengerViewModel
    private lateinit var binding: FragmentMessengerBinding

    companion object {
        const val MY_READ_PERMISSION_CODE = 1
    }

    private var messengerAdapter: MessengerAdapter? = null
    private var selectImageAdapter: SelectImageAdapter? = null
    private var stickerAdapter: StickerAdapter? = null
    private var isCheckedGallery = false
    private var isCheckedSticker = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMessengerBinding.inflate(inflater, container, false).apply {
            viewModel =
                ViewModelProviders.of(this@MessengerFragment).get(MessengerViewModel::class.java)
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        hideKeyboard()
        setupObservers()
        initAdapter()
        setListeners()
        view.setOnTouchListener { _, _ ->
            binding.selectImage.setImageResource(R.drawable.ic_add_photo_gray)
            binding.imgSticker.setImageResource(R.drawable.ic_smile_gray)
            binding.layoutSticker.visibility = View.GONE
            binding.layoutGallery.visibility = View.GONE
            isCheckedSticker = false
            isCheckedGallery = false
            hideSoftKeyboard(requireActivity())
            false
        }
    }

    private fun setListeners() {
        binding.imgBackMessenger.setOnClickListener {
            back()
            viewModel.updateStatusTyping("noOne")
            hideKeyboard()
        }
        binding.edtSend.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (s.isNotEmpty()) {
                    binding.imgSend.setImageResource(R.drawable.ic_send)
                } else {
                    binding.imgSend.setImageResource(R.drawable.ic_send_gray)
                }
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun afterTextChanged(s: Editable) {}
        })
        binding.imgSend.setOnClickListener {
            val msg: String = binding.edtSend.text.toString()
            if (msg.isNotEmpty()) {
                viewModel.sendMessage(msg, context)
            } else {
                Toast.makeText(activity, R.string.cant_send, Toast.LENGTH_SHORT).show()
            }
            binding.edtSend.setText("")
        }

        binding.recyclerViewMessenger.setOnClickListener {
            hideSoftKeyboard(requireActivity())
        }

        binding.selectImage.setOnClickListener {
            hideSoftKeyboard(requireActivity())
            if (!isCheckedGallery) {
                binding.selectImage.setImageResource(R.drawable.ic_add_photo)
                binding.imgSticker.setImageResource(R.drawable.ic_smile_gray)
                binding.layoutGallery.visibility = View.VISIBLE
                binding.layoutSticker.visibility = View.GONE
                requestPermission()
                hideKeyboard()
                isCheckedGallery = true
                isCheckedSticker = false
            } else {
                binding.layoutGallery.visibility = View.GONE
                binding.selectImage.setImageResource(R.drawable.ic_add_photo_gray)
                isCheckedGallery = false
            }
        }

        binding.btnSend.setOnClickListener {
            viewModel.sendImage(selectImageAdapter!!.getListImg(), context)
            selectImageAdapter!!.clearList()
            binding.btnCancel.visibility = View.GONE
            binding.btnSend.visibility = View.GONE
        }

        binding.btnCancel.setOnClickListener {
            binding.btnCancel.visibility = View.GONE
            binding.btnSend.visibility = View.GONE
            selectImageAdapter!!.clearList()
        }

        binding.imgSticker.setOnClickListener {
            hideSoftKeyboard(requireActivity())
            if (!isCheckedSticker) {
                binding.imgSticker.setImageResource(R.drawable.ic_smile)
                binding.selectImage.setImageResource(R.drawable.ic_add_photo_gray)
                binding.layoutSticker.visibility = View.VISIBLE
                binding.layoutGallery.visibility = View.GONE
                loadSticker()
                hideKeyboard()
                isCheckedSticker = true
                isCheckedGallery = false
            } else {
                binding.imgSticker.setImageResource(R.drawable.ic_smile_gray)
                binding.layoutSticker.visibility = View.GONE
                isCheckedSticker = false
            }
        }

        binding.edtSend.setOnClickListener {
            binding.selectImage.setImageResource(R.drawable.ic_add_photo_gray)
            binding.imgSticker.setImageResource(R.drawable.ic_smile_gray)
            binding.layoutSticker.visibility = View.GONE
            binding.layoutGallery.visibility = View.GONE
            isCheckedSticker = false
            isCheckedGallery = false
        }

        binding.edtSend.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s.toString().trim().isEmpty()) {
                    viewModel.updateStatusTyping("noOne")
                } else {
                    viewModel.sender!!.id?.let { viewModel.updateStatusTyping(it) }
                }
            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun initAdapter() {
        val linearLayoutManager = LinearLayoutManager(context)
        linearLayoutManager.stackFromEnd = true
        binding.recyclerViewMessenger.layoutManager = linearLayoutManager
        binding.recyclerViewMessenger.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    val lm: LayoutManager =
                        binding.recyclerViewMessenger.layoutManager as LinearLayoutManager
                    if (lm is LinearLayoutManager) {
                        val currPosition = lm.findFirstVisibleItemPosition()
                        var time: String? = null
                        try {
                            time = messengerAdapter!!.getTime(currPosition)
                        } catch (e: ParseException) {
                            e.printStackTrace()
                        }
                        binding.txtTimeChat.visibility = View.VISIBLE
                        binding.txtTimeChat.text = time
                    }
                }
            }
        })
        if (arguments != null && requireArguments().containsKey(Constants.Bundle.BUNDLE_KEY_ROOM)) {
            val room = requireArguments().getSerializable(Constants.Bundle.BUNDLE_KEY_ROOM) as Room?
            if (room != null) {
                viewModel.setRoom(room)
                binding.txtUsernameMessenger.text = room.name
                if (context != null) {
                    Glide.with(requireContext()).load(
                        if (room.thumbnail.equals(Constants.Firebase.FIRE_BASE_DEFAULT))
                            R.drawable.ic_avt
                        else
                            room.thumbnail
                    )
                        .into(binding.imgCircleMessenger)
                }
                messengerAdapter = MessengerAdapter(requireContext(), java.util.ArrayList(), room)
                binding.recyclerViewMessenger.adapter = messengerAdapter
                if (room.thumbnail == Constants.Firebase.FIRE_BASE_DEFAULT)
                    Glide.with(requireContext()).load(R.drawable.ic_avt).into(binding.imgCircleShowMess)
                else
                    Glide.with(requireContext()).load(room.thumbnail).into(binding.imgCircleShowMess)
                messengerAdapter!!.setOnLongClickListener(object : OnLongClickListener {
                    override fun transferMessageText(position: Int, content: String) {
                        showDialog(room, content)
                    }

                    override fun transferMessageImage(position: Int, content: String) {
                        showDialog(room, content)
                    }

                })
                viewModel.isShowTyping.observe(viewLifecycleOwner) {
                    if (it) {
                        binding.layoutTyping.visibility = View.VISIBLE
                        binding.txtShowMess.text = room.name + requireContext().getString(R.string.typing)
                    } else {
                        binding.layoutTyping.visibility = View.GONE
                    }
                }
            }
        }
    }

    private fun showDialog(room: Room, content: String) {
        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val bindingDialog: DialogTransferMessageBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context), R.layout.dialog_transfer_message, null, false
        )
        dialog.setContentView(bindingDialog.root)
        val window = dialog.window
        window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val windowAttribute: WindowManager.LayoutParams = window?.attributes!!
        windowAttribute.gravity = Gravity.CENTER
        window.attributes = windowAttribute
        bindingDialog.btnTransfer.setOnClickListener {
            val bundle = Bundle()
            bundle.putSerializable(Constants.Bundle.BUNDLE_CONTENT, content)
            NavHostFragment.findNavController(this@MessengerFragment)
                .navigate(R.id.action_messengerFragment_to_transferMessageFragment, bundle)
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun setupObservers() {
        viewModel.resultMessage.observe(viewLifecycleOwner) {
            it?.let {
                if (messengerAdapter != null) {
                    messengerAdapter!!.setData(it as ArrayList<Message>)
                }
            }
        }
    }

    private fun back() {
        Navigation.findNavController(requireView()).navigateUp()
    }

    private fun requestPermission() {
        if (ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                (context as Activity?)!!,
                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                MY_READ_PERMISSION_CODE
            )
        } else {
            loadImages()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == MY_READ_PERMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(activity, "Success", Toast.LENGTH_SHORT).show()
                loadImages()
            } else {
                Toast.makeText(activity, "Fail", Toast.LENGTH_SHORT).show()
            }
        }
    }


    private fun loadImages() {
        val gridLayoutManager = GridLayoutManager(context, 3, LinearLayoutManager.VERTICAL, false)
        binding.recyclerViewGallery.layoutManager = gridLayoutManager
        binding.recyclerViewGallery.isFocusable = false
        val listImage: List<String> = ImageGallery.lisOfImages(requireActivity())
        selectImageAdapter = SelectImageAdapter(requireContext(), listImage)
        binding.recyclerViewGallery.adapter = selectImageAdapter
        selectImageAdapter!!.setOnItemGalleryClick(object : SelectImageAdapter.ClickItemGallery {
            override fun onClickItemGallery(image: String?, position: Int) {
                if (selectImageAdapter!!.getListImg().size > 0) {
                    binding.btnCancel.visibility = View.VISIBLE
                    binding.btnSend.visibility = View.VISIBLE
                } else {
                    binding.btnCancel.visibility = View.GONE
                    binding.btnSend.visibility = View.GONE
                }
            }
        })
    }

    private fun loadSticker() {
        val gridLayoutManager = GridLayoutManager(context, 3, LinearLayoutManager.VERTICAL, false)
        binding.recyclerViewSticker.layoutManager = gridLayoutManager
        binding.recyclerViewSticker.isFocusable = false
        val listSticker: ArrayList<String> = ArrayList(Constants.stickers)
        stickerAdapter = StickerAdapter(requireContext(), listSticker)
        binding.recyclerViewSticker.adapter = stickerAdapter
        stickerAdapter!!.setOnItemStickerClick(object : StickerAdapter.ClickItemSticker {
            override fun onClickItemSticker(sticker: String?, position: Int) {
                viewModel.sendSticker(sticker, context)
            }
        })
    }

    override fun onResume() {
        super.onResume()
        activity.let {
            it?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        }
    }

    override fun onStop() {
        super.onStop()
        activity.let {
            it?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        }
    }
}