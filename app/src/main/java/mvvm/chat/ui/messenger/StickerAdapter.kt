package mvvm.chat.ui.messenger

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.mvvm.databinding.ItemSelectStickerBinding
import com.bumptech.glide.Glide

class StickerAdapter(private val context: Context, private var list: ArrayList<String>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var clickItemSticker: ClickItemSticker? = null

    interface ClickItemSticker {
        fun onClickItemSticker(sticker: String?, position: Int)
    }

    fun setOnItemStickerClick(clickItemSticker: ClickItemSticker?) {
        this.clickItemSticker = clickItemSticker
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): RecyclerView.ViewHolder {
        return MyViewHolder(
            ItemSelectStickerBinding.inflate(LayoutInflater.from(viewGroup.context), viewGroup, false)
        )
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, i: Int) {
        val sticker = list[i]
        val myViewHolder = viewHolder as MyViewHolder
        myViewHolder.bindView(sticker, clickItemSticker, context)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class MyViewHolder(val binding: ItemSelectStickerBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bindView(sticker: String?, clickItemSticker: ClickItemSticker?, context: Context?) {
            Glide.with(context!!).load(sticker).into(binding.imgFormListSticker)
            binding.imgFormListSticker.setOnClickListener {
                clickItemSticker?.onClickItemSticker(sticker, adapterPosition)
            }
        }
    }

}