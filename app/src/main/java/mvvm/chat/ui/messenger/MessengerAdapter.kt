package mvvm.chat.ui.messenger

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.mvvm.R
import com.android.mvvm.databinding.*
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import mvvm.chat.data.Message
import mvvm.chat.data.Room
import mvvm.chat.utils.Constants
import mvvm.chat.utils.DateUtils
import mvvm.chat.utils.listener.OnLongClickListener
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class MessengerAdapter(
    private val context: Context,
    private var data: ArrayList<Message>,
    private var room: Room
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    companion object {
        const val MSG_TYPE_RIGHT = 1
        const val MSG_TYPE_LEFT = 0
        const val IMAGE_LEFT = 2
        const val IMAGE_RIGHT = 3
    }

    private var firebaseUser: FirebaseUser? = null
    private var onLongClickListener: OnLongClickListener? = null

    fun setOnLongClickListener(onLongClickListener: OnLongClickListener?) {
        this.onLongClickListener = onLongClickListener
    }


    @SuppressLint("SimpleDateFormat")
    var sdfFull = SimpleDateFormat(Constants.TimeFormat.FULL)

    @SuppressLint("NotifyDataSetChanged")
    fun setData(data: ArrayList<Message>) {
        this.data = data
        notifyDataSetChanged()
    }

    @Throws(ParseException::class)
    fun getTime(pos: Int): String? {
        val time = sdfFull.parse(data[pos].time)
        val timeMess = DateUtils(time)
        val calendar = DateUtils(Calendar.getInstance().time)
        return if (timeMess.getMonth().equals(calendar.getMonth())) {
            val i: Int = calendar.getDay()!!.toInt() - timeMess.getDay()!!.toInt()
            when {
                i == 0 -> {
                    context.getString(R.string.now)
                }
                i == 1 -> {
                    context.getString(R.string.yesterday)
                }
                i > 1 -> {
                    timeMess.getDate()
                }
                else -> {
                    context.getString(R.string.now)
                }
            }
        } else {
            timeMess.getDate()
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): RecyclerView.ViewHolder {
        return when (i) {
            MSG_TYPE_RIGHT -> MyViewHolderMessRight(
                ItemMessRightBinding.inflate(
                    LayoutInflater.from(viewGroup.context), viewGroup, false
                )
            )
            MSG_TYPE_LEFT -> MyViewHolderMessLeft(
                ItemMessLeftBinding.inflate(
                    LayoutInflater.from(viewGroup.context), viewGroup, false
                )
            )
            IMAGE_LEFT -> MyViewHolderImgLeft(
                ItemImageLeftBinding.inflate(
                    LayoutInflater.from(viewGroup.context), viewGroup, false
                )
            )
            IMAGE_RIGHT -> MyViewHolderImgRight(
                ItemImageRightBinding.inflate(
                    LayoutInflater.from(viewGroup.context), viewGroup, false
                )
            )
            else -> throw IllegalArgumentException("Invalid view type")
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, i: Int) {
        val message: Message = data[i]
        try {
            when (holder) {
                is MyViewHolderMessRight -> holder.bindView(
                    sdfFull,
                    room,
                    message,
                    data,
                    context,
                    onLongClickListener
                )
                is MyViewHolderMessLeft -> holder.bindView(
                    sdfFull,
                    room,
                    message,
                    data,
                    context,
                    onLongClickListener
                )
                is MyViewHolderImgRight -> holder.bindView(
                    sdfFull,
                    room,
                    message,
                    data,
                    context,
                    onLongClickListener
                )
                is MyViewHolderImgLeft -> holder.bindView(
                    sdfFull,
                    room,
                    message,
                    data,
                    context,
                    onLongClickListener
                )
            }
        } catch (e: ParseException) {
            e.printStackTrace()
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    class MyViewHolderMessRight(val binding: ItemMessRightBinding) :
        RecyclerView.ViewHolder(binding.root) {
        @Throws(ParseException::class)
        fun bindView(
            sdfFull: SimpleDateFormat,
            room: Room,
            message: Message,
            data: ArrayList<Message>,
            context: Context,
            onLongClickListener: OnLongClickListener?
        ) {
            if (message.content!!.startsWith("https://")) {
                Glide.with(context).load(message.content).apply(RequestOptions().override(300, 200))
                    .into(binding.showMessImage)
            } else {
                binding.txtShowMess.text = message.content
            }
            if (room.thumbnail == Constants.Firebase.FIRE_BASE_DEFAULT)
                Glide.with(context).load(R.drawable.ic_avt).into(binding.imgCircleShowMess)
            else
                Glide.with(context).load(room.thumbnail).into(binding.imgCircleShowMess)
            val time: Date = message.time?.let { sdfFull.parse(it) } as Date
            val timeMess = DateUtils(time)
            if (adapterPosition == data.size - 1) {
                binding.txtTime.visibility = View.VISIBLE
                binding.txtTime.text = timeMess.getHour()
            } else {
                binding.txtTime.visibility = View.GONE
            }
            binding.txtShowMess.setOnLongClickListener {
                onLongClickListener?.transferMessageText(
                    adapterPosition,
                    binding.txtShowMess.text.toString()
                )
                false
            }
        }
    }

    class MyViewHolderMessLeft(val binding: ItemMessLeftBinding) :
        RecyclerView.ViewHolder(binding.root) {
        @Throws(ParseException::class)
        fun bindView(
            sdfFull: SimpleDateFormat,
            room: Room,
            message: Message,
            data: ArrayList<Message>,
            context: Context,
            onLongClickListener: OnLongClickListener?
        ) {
            if (message.content!!.startsWith("https://")) {
                Glide.with(context).load(message.content).apply(RequestOptions().override(300, 200))
                    .into(binding.showMessImage)
            } else {
                binding.txtShowMess.text = message.content
            }
            if (room.thumbnail == Constants.Firebase.FIRE_BASE_DEFAULT)
                Glide.with(context).load(R.drawable.ic_avt).into(binding.imgCircleShowMess)
            else
                Glide.with(context).load(room.thumbnail).into(binding.imgCircleShowMess)
            val time: Date = sdfFull.parse(message.time)
            val timeMess = DateUtils(time)
            if (adapterPosition == data.size - 1) {
                binding.txtTime.visibility = View.VISIBLE
                binding.txtTime.text = timeMess.getHour()
            } else {
                binding.txtTime.visibility = View.GONE
            }
            binding.txtShowMess.setOnLongClickListener {
                onLongClickListener?.transferMessageText(
                    adapterPosition,
                    binding.txtShowMess.text.toString()
                )
                false
            }
        }
    }

    class MyViewHolderImgRight(val binding: ItemImageRightBinding) :
        RecyclerView.ViewHolder(binding.root) {
        @Throws(ParseException::class)
        fun bindView(
            sdfFull: SimpleDateFormat,
            room: Room,
            message: Message,
            data: ArrayList<Message>,
            context: Context,
            onLongClickListener: OnLongClickListener?
        ) {
            if (message.content!!.startsWith("https://")) {
                Glide.with(context).load(message.content).apply(RequestOptions().override(300, 200))
                    .into(binding.showMessImage)
            } else {
                binding.txtShowMess.text = message.content
            }
            if (room.thumbnail == Constants.Firebase.FIRE_BASE_DEFAULT)
                Glide.with(context).load(R.drawable.ic_avt).into(binding.imgCircleShowMess)
            else
                Glide.with(context).load(room.thumbnail).into(binding.imgCircleShowMess)
            val time: Date = sdfFull.parse(message.time)
            val timeMess = DateUtils(time)
            if (adapterPosition == data.size - 1) {
                binding.txtTime.visibility = View.VISIBLE
                binding.txtTime.text = timeMess.getHour()
            } else {
                binding.txtTime.visibility = View.GONE
            }
            binding.txtShowMess.setOnLongClickListener {
                onLongClickListener?.transferMessageImage(adapterPosition, message.content!!)
                false
            }
        }
    }

    class MyViewHolderImgLeft(val binding: ItemImageLeftBinding) :
        RecyclerView.ViewHolder(binding.root) {
        @Throws(ParseException::class)
        fun bindView(
            sdfFull: SimpleDateFormat,
            room: Room,
            message: Message,
            data: ArrayList<Message>,
            context: Context,
            onLongClickListener: OnLongClickListener?
        ) {
            if (message.content!!.startsWith("https://")) {
                Glide.with(context).load(message.content).apply(RequestOptions().override(300, 200))
                    .into(binding.showMessImage)
            } else {
                binding.txtShowMess.text = message.content
            }
            if (room.thumbnail == Constants.Firebase.FIRE_BASE_DEFAULT)
                Glide.with(context).load(R.drawable.ic_avt).into(binding.imgCircleShowMess)
            else
                Glide.with(context).load(room.thumbnail).into(binding.imgCircleShowMess)
            val time: Date = sdfFull.parse(message.time)
            val timeMess = DateUtils(time)
            if (adapterPosition == data.size - 1) {
                binding.txtTime.visibility = View.VISIBLE
                binding.txtTime.text = timeMess.getHour()
            } else {
                binding.txtTime.visibility = View.GONE
            }
            binding.txtShowMess.setOnLongClickListener {
                onLongClickListener?.transferMessageImage(adapterPosition, message.content!!)
                false
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        firebaseUser = FirebaseAuth.getInstance().currentUser
        return if (data[position].senderId.equals(firebaseUser!!.uid)) {
            if (data[position].content!!.startsWith("https://")) {
                IMAGE_RIGHT
            } else {
                MSG_TYPE_RIGHT
            }
        } else {
            if (data[position].content!!.startsWith("https://")) {
                IMAGE_LEFT
            } else {
                MSG_TYPE_LEFT
            }
        }
    }
}