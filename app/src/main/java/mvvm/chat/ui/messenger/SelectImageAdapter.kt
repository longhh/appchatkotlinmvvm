package mvvm.chat.ui.messenger

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.mvvm.databinding.ItemSelectImageBinding
import com.bumptech.glide.Glide
import java.io.File

class SelectImageAdapter(private val context: Context, private var list: List<String>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val listImg = ArrayList<String>()
    private var clickItemGallery: ClickItemGallery? = null


    fun getListImg(): ArrayList<String> {
        return listImg
    }

    fun clearList() {
        listImg.clear()
    }

    interface ClickItemGallery {
        fun onClickItemGallery(image: String?, position: Int)
    }

    fun setOnItemGalleryClick(clickItemGallery: ClickItemGallery?) {
        this.clickItemGallery = clickItemGallery
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): MyViewHolder {
        return MyViewHolder(
            ItemSelectImageBinding.inflate(LayoutInflater.from(viewGroup.context), viewGroup, false)
        )
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, i: Int) {
        val image = list[i]
        val myViewHolder = viewHolder as MyViewHolder
        myViewHolder.bindView(image, clickItemGallery, listImg, context)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class MyViewHolder(val binding: ItemSelectImageBinding) : RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bindView(image: String?, clickItemGallery: ClickItemGallery?, listImg: ArrayList<String>, context: Context?) {
            Glide.with(context!!).load(File(image)).into(binding.imgFromGallery)
            binding.imgFromGallery.setOnClickListener {
                if (binding.checkSelect.visibility == View.VISIBLE) {
                    binding.checkSelect.visibility = View.GONE
                } else {
                    binding.checkSelect.visibility = View.VISIBLE
                }
                if (listImg.contains(image)) {
                    listImg.remove(image)
                } else {
                    listImg.add(image!!)
                }
                clickItemGallery?.onClickItemGallery(image, adapterPosition)
                binding.checkSelect.text = listImg.size.toString()
            }
        }
    }
}