package mvvm.chat.ui.messenger

import android.annotation.SuppressLint
import android.content.Context
import android.net.Uri
import androidx.lifecycle.MutableLiveData
import com.android.mvvm.R
import mvvm.chat.base.BaseViewModel
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import mvvm.chat.data.Message
import mvvm.chat.data.Room
import mvvm.chat.data.Users
import mvvm.chat.fcm.*
import mvvm.chat.model.api.APIFirebaseService
import mvvm.chat.utils.Constants
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class MessengerViewModel : BaseViewModel() {
    val resultMessage = MutableLiveData<List<Message>>()
    val isShowTyping = MutableLiveData(false)
    var sender: Users? = null
    private var room: Room? = null
    val uid: String = Firebase.auth.currentUser!!.uid

    var apiService: APIFirebaseService = Client.getClient("https://fcm.googleapis.com/")!!.create(
        APIFirebaseService::class.java
    )

    fun setRoom(room: Room?) {
        this.room = room
        getDataSender()
        getDataChatRoom()
        checkStatusTyping()
    }

    private fun getDataSender() {
        FirebaseDatabase.getInstance()
            .getReference(Constants.Firebase.FIRE_BASE_USERS)
            .child(uid)
            .addValueEventListener(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    sender = dataSnapshot.getValue(Users::class.java)
                }

                override fun onCancelled(databaseError: DatabaseError) {
                }
            })
    }

    private fun getDataChatRoom() {
        FirebaseDatabase.getInstance()
            .getReference(Constants.Reference.REFERENCE_MESSAGES)
            .child(room!!.id!!)
            .addValueEventListener(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    val messages = ArrayList<Message>()
                    for (snapshot in dataSnapshot.children) {
                        val message = snapshot.getValue(Message::class.java)
                        if (message != null) {
                            messages.add(message)
                        }
                    }
                    resultMessage.postValue(messages)
                }

                override fun onCancelled(databaseError: DatabaseError) {
                }
            })
        // Reset unread message
        FirebaseDatabase.getInstance()
            .getReference(Constants.Reference.REFERENCE_ROOMS)
            .child(uid)
            .child(room!!.id!!)
            .child(Constants.Firebase.UNREAD_MESSAGE)
            .setValue(0)
            .addOnSuccessListener { room!!.unreadMessage = 0 }
    }

    fun sendMessage(message: String, context: Context?) {
        val newMessage = Message(sender!!.id, message, getCurrentTime())
        FirebaseDatabase.getInstance()
            .getReference(Constants.Reference.REFERENCE_MESSAGES)
            .child(room!!.id!!)
            .push()
            .setValue(newMessage)
            .addOnSuccessListener { updateChatRoom(message) }
        sendNotification(message, context)
    }

    private fun sendNotification(message: String, context: Context?) {
        FirebaseDatabase.getInstance()
            .getReference(Constants.Reference.REFERENCE_TOKENS)
            .orderByKey()
            .equalTo(room!!.receiver)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    for (dataSnapshot in snapshot.children) {
                        val token = dataSnapshot.getValue(Token::class.java)
                        val data = Data(
                            uid,
                            R.mipmap.ic_launcher,
                            sender!!.username.toString() + " : " + message,
                            context!!.getString(R.string.new_message),
                            room!!.receiver!!
                        )
                        assert(token != null)
                        val sender = Sender(data, token!!.token)
                        apiService.sendNotifications(sender)
                            ?.enqueue(object : Callback<MyResponse?> {
                                override fun onResponse(
                                    call: Call<MyResponse?>,
                                    response: Response<MyResponse?>
                                ) {
                                    if (response.code() == 200) {
                                        assert(response.body() != null)
                                    }
                                }

                                override fun onFailure(call: Call<MyResponse?>, t: Throwable) {}
                            })
                    }
                }

                override fun onCancelled(error: DatabaseError) {}
            })
    }

    fun sendImage(listImg: ArrayList<String>?, context: Context?) {
        val reference = FirebaseStorage.getInstance().reference
        for (i in listImg!!.indices) {
            val file = Uri.fromFile(File(listImg[i]))
            val riversRef = reference.child("images/" + file.lastPathSegment)
            riversRef.putFile(file).addOnSuccessListener {
                riversRef.downloadUrl.addOnSuccessListener { uri ->
                    val newMessage =
                        Message(sender!!.id, uri.toString(), getCurrentTime())
                    FirebaseDatabase.getInstance()
                        .getReference(Constants.Reference.REFERENCE_MESSAGES)
                        .child(room!!.id!!)
                        .push()
                        .setValue(newMessage)
                        .addOnSuccessListener {
                            val receiver: String = sender!!.username + context!!.getString(R.string.send_image)
                            updateChatRoom(context.getString(R.string.you_send_image), receiver)
                        }
                }
            }
        }
        sendNotification(context!!.getString(R.string.send_image), context)
    }

    fun sendSticker(sticker: String?, context: Context?) {
        val newMessage = Message(sender!!.id, sticker, getCurrentTime())
        FirebaseDatabase.getInstance()
            .reference
            .child(Constants.Reference.REFERENCE_MESSAGES)
            .child(room!!.id!!)
            .push()
            .setValue(newMessage)
            .addOnSuccessListener {
                val receiver: String =
                    sender!!.username.toString() + " " + context!!.getString(R.string.send_sticker)
                updateChatRoom(context.getString(R.string.you_send_sticker), receiver)
            }
        sendNotification(context!!.getString(R.string.send_sticker), context)
    }

    private fun updateChatRoom(message: String) {
        room!!.lastMessage = message
        room!!.timestamp = getTimestamp()
        //Update chat room sender
        FirebaseDatabase.getInstance()
            .getReference(Constants.Reference.REFERENCE_ROOMS)
            .child(uid)
            .child(room!!.id!!)
            .setValue(room)

        //Update chat room receiver
        FirebaseDatabase.getInstance()
            .getReference(Constants.Reference.REFERENCE_ROOMS)
            .child(room!!.receiver!!)
            .child(room!!.id!!)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    val room = dataSnapshot.getValue(Room::class.java)
                    if (room != null) {
                        room.lastMessage = message
                        val unread: Int = room.unreadMessage + 1
                        room.name = sender!!.username
                        room.thumbnail = sender!!.imageURL
                        room.searchByName = sender!!.username!!.toLowerCase()
                        room.unreadMessage = unread
                        room.timestamp = getTimestamp()
                        dataSnapshot.ref.setValue(room)
                    }
                }

                override fun onCancelled(databaseError: DatabaseError) {
               }
            })
    }

    private fun updateChatRoom(sender: String, receiver: String) {
        room!!.lastMessage = sender
        room!!.timestamp = getTimestamp()

        //Update chat room sender
        FirebaseDatabase.getInstance()
            .getReference(Constants.Reference.REFERENCE_ROOMS)
            .child(uid)
            .child(room!!.id!!)
            .setValue(room)

        //Update chat room receiver
        FirebaseDatabase.getInstance()
            .getReference(Constants.Reference.REFERENCE_ROOMS)
            .child(room!!.receiver!!)
            .child(room!!.id!!)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    val room = dataSnapshot.getValue(Room::class.java)
                    if (room != null) {
                        room.lastMessage = receiver
                        val unread: Int = room.unreadMessage + 1
                        room.unreadMessage = unread
                        room.timestamp = getTimestamp()
                        dataSnapshot.ref.setValue(room)
                    }
                }

                override fun onCancelled(databaseError: DatabaseError) {
                }
            })
    }

    @SuppressLint("SimpleDateFormat")
    private fun getCurrentTime(): String? {
        val date = Calendar.getInstance().time
        val df = SimpleDateFormat("dd-MM-yyyy HH:mm:ss")
        return df.format(date)
    }

    private fun getTimestamp(): String {
        val tsLong = System.currentTimeMillis()
        return tsLong.toString()
    }

    fun videoCall(context: Context?) {
        sendNotificationVideoCall(context)
    }

    private fun sendNotificationVideoCall(context: Context?) {
        FirebaseDatabase.getInstance()
            .getReference(Constants.Reference.REFERENCE_TOKENS)
            .orderByKey()
            .equalTo(room!!.receiver)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    for (dataSnapshot in snapshot.children) {
                        val token = dataSnapshot.getValue(Token::class.java)
                        val data = Data(
                            uid, R.mipmap.ic_launcher, sender!!.username.toString() + " gọi từ AwesomeChat",
                            context!!.getString(R.string.text_noti), room!!.receiver!!
                        )
                        assert(token != null)
                        val sender = Sender(data, token!!.token)
                        apiService.sendNotifications(sender)
                            ?.enqueue(object : Callback<MyResponse?> {
                                override fun onResponse(
                                    call: Call<MyResponse?>,
                                    response: Response<MyResponse?>
                                ) {
                                    if (response.code() == 200) {
                                        assert(response.body() != null)
                                    }
                                }

                                override fun onFailure(call: Call<MyResponse?>, t: Throwable) {}
                            })
                    }
                }

                override fun onCancelled(error: DatabaseError) {}
            })
    }

    fun updateStatusTyping(id: String) {
        val map = HashMap<String, Any>()
        map[Constants.Firebase.FIRE_BASE_TYPING] = id
        val user = Firebase.auth.currentUser
        user?.let {
            val uid: String = user.uid
            FirebaseDatabase.getInstance().getReference(Constants.Reference.REFERENCE_ROOMS)
                .child(uid)
                .child(room!!.id!!)
                .updateChildren(map)

            FirebaseDatabase.getInstance().getReference(Constants.Reference.REFERENCE_ROOMS)
                .child(room!!.receiver!!)
                .child(room!!.id!!)
                .updateChildren(map)
        }
    }

    private fun checkStatusTyping() {
        val user = Firebase.auth.currentUser
        user?.let {
            val uid: String = user.uid
            FirebaseDatabase.getInstance().getReference(Constants.Reference.REFERENCE_ROOMS)
                .child(uid)
                .child(room!!.id!!)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        room = dataSnapshot.getValue(Room::class.java)
                        if (room!!.typing.equals(uid) || room!!.typing.equals("noOne")) {
                            isShowTyping.postValue(false)
                        } else {
                            isShowTyping.postValue(true)
                        }
                    }

                    override fun onCancelled(databaseError: DatabaseError) {
                    }
                })

        }
    }
}