package mvvm.chat.ui.homemessenger

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.mvvm.R
import com.android.mvvm.databinding.ItemHomeMessengerBinding
import com.bumptech.glide.Glide
import mvvm.chat.data.Room
import mvvm.chat.utils.Constants
import mvvm.chat.utils.DateUtilsLongTime
import java.text.ParseException

class HomeMessengerAdapter(private val context: Context, private var data: ArrayList<Room>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var clickItemView: ClickItemView? = null

    @SuppressLint("NotifyDataSetChanged")
    fun setData(data: ArrayList<Room>) {
        this.data = data
        notifyDataSetChanged()
    }

    fun getData(): ArrayList<Room> {
        return data
    }

    interface ClickItemView {
        fun onClickItem(room: Room?, position: Int)
    }

    fun setOnItemClick(clickItemView: ClickItemView?) {
        this.clickItemView = clickItemView
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): RecyclerView.ViewHolder {
        return MyViewHolder(
            ItemHomeMessengerBinding.inflate(
                LayoutInflater.from(viewGroup.context),
                viewGroup,
                false
            )
        )
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, i: Int) {
        val room: Room = data[i]
        try {
            val myViewHolder = viewHolder as MyViewHolder
            myViewHolder.bindView(room, clickItemView, context)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
    }


    override fun getItemCount(): Int {
        return data.size
    }

    class MyViewHolder(val binding: ItemHomeMessengerBinding) : RecyclerView.ViewHolder(binding.root) {
        @Throws(ParseException::class)
        fun bindView(room: Room, clickItemView: ClickItemView?, context: Context) {
            binding.txtName.text = room.name
            val current = DateUtilsLongTime(System.currentTimeMillis())
            val time = room.timestamp?.let { DateUtilsLongTime(it.toLong()) }
            val results: Int = current.getDayLong()!!.toInt() - time?.getDayLong()!!.toInt()
            if (current.getMonthLong().equals(time.getMonthLong())) {
                when {
                    results == 1 -> {
                        binding.txtTime.text = context.getString(R.string.yesterday)
                    }
                    results > 1 -> {
                        binding.txtTime.text = time.getDateLong()
                    }
                    else -> {
                        binding.txtTime.text = time.getHourLong()
                    }
                }
            } else {
                binding.txtTime.text = time.getDateLong()
            }
            if (room.unreadMessage > 0) {
                binding.txtUnreadMessage.visibility = View.VISIBLE
                if (room.unreadMessage > 9) {
                    binding.txtUnreadMessage.text = "+9"
                } else {
                    binding.txtUnreadMessage.text = room.unreadMessage.toString()
                }
            } else {
                binding.txtUnreadMessage.visibility = View.GONE
            }
            binding.txtLastMessage.text = room.lastMessage
            if (room.thumbnail.equals(Constants.Firebase.FIRE_BASE_DEFAULT)) {
                Glide.with(context).load(R.drawable.ic_avt).into(binding.imgCircleHomeMess)
            } else {
                Glide.with(context).load(room.thumbnail).into(binding.imgCircleHomeMess)
            }
            binding.itemHomeMessenger.setOnClickListener {
                clickItemView?.onClickItem(room, adapterPosition)
            }
        }
    }

}