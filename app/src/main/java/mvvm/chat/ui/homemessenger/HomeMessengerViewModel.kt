package mvvm.chat.ui.homemessenger

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import mvvm.chat.base.BaseViewModel
import mvvm.chat.data.Room
import mvvm.chat.utils.Constants

class HomeMessengerViewModel : BaseViewModel() {
    val resultRoom = MutableLiveData<ArrayList<Room>>()
    val progressBar = MutableLiveData<Boolean>()

    init {
        getListChatRoom()
    }

    private fun getListChatRoom() {
        val handlerException = CoroutineExceptionHandler { _, throwable ->
            println("CoroutineExceptionHandler got $throwable")
        }
        CoroutineScope(Dispatchers.IO + handlerException).launch {
            progressBar.postValue(true)
            try {
                val user = Firebase.auth.currentUser
                user?.let {
                    val uid: String = it.uid
                    FirebaseDatabase.getInstance()
                        .getReference(Constants.Reference.REFERENCE_ROOMS)
                        .child(uid)
                        .addValueEventListener(object : ValueEventListener {
                            override fun onDataChange(snapshot: DataSnapshot) {
                                val roomArrayList = ArrayList<Room>()
                                for (dataSnapshot in snapshot.children) {
                                    val room = dataSnapshot.getValue(Room::class.java)
                                    if (room != null && !room.lastMessage.equals("")) {
                                        roomArrayList.add(room)
                                    }
                                }
                                resultRoom.postValue(roomArrayList)
                                progressBar.postValue(false)

                            }

                            override fun onCancelled(error: DatabaseError) {
                            }
                        })
                }
            } catch (e: Exception) {
                Log.getStackTraceString(e)
                resultRoom.postValue(ArrayList())
            }
        }
    }

    fun searchByName(name: String) {
        val arrayList = ArrayList<Room>()
        val user = Firebase.auth.currentUser
        user?.let {
            val uid: String = it.uid
            FirebaseDatabase.getInstance()
                .getReference(Constants.Reference.REFERENCE_ROOMS)
                .child(uid)
                .orderByChild(Constants.Firebase.FIRE_BASE_SEARCH_BY_NAME)
                .startAt(name).endAt(name + "\uf8ff")
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        arrayList.clear()
                        for (dataSnapshot in snapshot.children) {
                            val room = dataSnapshot.getValue(Room::class.java)!!
                            if (!room.id.equals(uid)
                                && !room.lastMessage.equals("")
                            ) {
                                arrayList.add(room)
                            }
                        }
                        resultRoom.postValue(arrayList)
                    }

                    override fun onCancelled(error: DatabaseError) {}
                })
        }
    }
}