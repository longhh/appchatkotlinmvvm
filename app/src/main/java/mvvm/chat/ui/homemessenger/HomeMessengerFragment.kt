package mvvm.chat.ui.homemessenger

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.mvvm.R
import com.android.mvvm.databinding.FragmentHomeMessengerBinding
import mvvm.chat.base.BaseFragment
import mvvm.chat.data.Room
import mvvm.chat.utils.Constants
import java.util.*
import kotlin.collections.ArrayList

class HomeMessengerFragment : BaseFragment() {

    private lateinit var viewModel: HomeMessengerViewModel
    private lateinit var binding: FragmentHomeMessengerBinding

    private var adapter: HomeMessengerAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeMessengerBinding.inflate(inflater, container, false).apply {
            viewModel = ViewModelProviders.of(this@HomeMessengerFragment).get(HomeMessengerViewModel::class.java)
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupObservers()
        initAdapter()
        setListeners()
        view.setOnTouchListener { _, _ ->
            hideSoftKeyboard(requireActivity())
            false
        }
    }

    private fun setListeners() {
        binding.searchViewMess.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                viewModel.searchByName(charSequence.toString().toLowerCase(Locale.getDefault()))
            }

            override fun afterTextChanged(editable: Editable) {}
        })
    }

    private fun initAdapter() {
        binding.recyclerViewHomeMessenger.setHasFixedSize(true)
        adapter = HomeMessengerAdapter(requireContext(), ArrayList())
        binding.recyclerViewHomeMessenger.adapter = adapter

        adapter!!.setOnItemClick(object : HomeMessengerAdapter.ClickItemView {
            override fun onClickItem(room: Room?, position: Int) {
                openChatRoom(adapter!!.getData()[position])
            }
        })
    }

    private fun openChatRoom(room: Room) {
        val bundle = Bundle()
        bundle.putSerializable(Constants.Bundle.BUNDLE_KEY_ROOM, room)
        NavHostFragment.findNavController(this@HomeMessengerFragment).navigate(R.id.action_homeMessenger_to_messengerFragment, bundle)
    }

    private fun setupObservers() {
        viewModel.resultRoom.observe(viewLifecycleOwner) {
            it?.let {
                if (adapter != null) {
                    it.sortWith { room, t1 ->
                        if (room.timestamp!!.toLong() < t1.timestamp!!.toLong()) {
                            1
                        } else {
                            if (room.timestamp!!.toLong() == t1.timestamp!!.toLong()) {
                                0
                            } else {
                                -1
                            }
                        }
                    }
                    adapter!!.setData(it)
                }
            }
        }
        viewModel.progressBar.observe(viewLifecycleOwner) {
            if (it) {
                binding.progressBar.visibility = View.VISIBLE
            } else {
                binding.progressBar.visibility = View.GONE
            }
        }
    }
}