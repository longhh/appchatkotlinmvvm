package mvvm.chat.ui.login

import android.util.Log
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.MutableLiveData
import com.facebook.AccessToken
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.ktx.Firebase
import mvvm.chat.base.BaseViewModel
import mvvm.chat.utils.Constants
import java.util.*
import kotlin.collections.HashMap

class LoginViewModel : BaseViewModel() {
    private val firebaseAuth = FirebaseAuth.getInstance()
    val resultData = MutableLiveData(0)
    fun login(email: String, pass: String){
        val auth: FirebaseAuth = Firebase.auth
        auth.signInWithEmailAndPassword(email, pass)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    resultData.postValue(1)
                } else {
                    resultData.postValue(2)
                }
            }
    }

    fun handleFacebookAccessToken(fragmentActivity: FragmentActivity, accessToken: AccessToken) {
        val authCredential = FacebookAuthProvider.getCredential(accessToken.token)
        firebaseAuth.signInWithCredential(authCredential)
            .addOnCompleteListener(fragmentActivity) { task ->
                if (task.isSuccessful) {
                    val name: String = firebaseAuth.currentUser!!.displayName.toString()
                    val image: String = firebaseAuth.currentUser!!.photoUrl.toString() + "?type=large"
                    Log.d("Facebook Login", "signInWithCredential success")
                    val user = Firebase.auth.currentUser
                    user?.let {
                        val userid = user.uid
                        val hashMap = HashMap<String, String>()
                        hashMap[Constants.Firebase.FIRE_BASE_ID] = userid
                        hashMap[Constants.Firebase.FIRE_BASE_USERNAME] = name
                        hashMap[Constants.Firebase.FIRE_BASE_IMAGE_URL] = image
                        hashMap[Constants.Firebase.FIRE_BASE_SEARCH] = name.toLowerCase(Locale.getDefault())
                        hashMap[Constants.Firebase.FIRE_BASE_PHONE_NUMBER] = Constants.Firebase.FIRE_BASE_DEFAULT
                        hashMap[Constants.Firebase.FIRE_BASE_DATE_OF_BIRTH] = Constants.Firebase.FIRE_BASE_DEFAULT
                        hashMap[Constants.Firebase.FIRE_BASE_ADDRESS] = Constants.Firebase.FIRE_BASE_DEFAULT

                        FirebaseDatabase.getInstance()
                            .getReference(Constants.Firebase.FIRE_BASE_USERS)
                            .child(userid)
                            .setValue(hashMap)
                            .addOnCompleteListener { task ->
                                if (task.isSuccessful) {
                                    resultData.postValue(1)
                                } else {
                                    resultData.postValue(2)
                                }
                            }
                    }
                } else {
                    Log.d("Facebook Login", "signInWithCredential failed")
                }
            }
    }
}