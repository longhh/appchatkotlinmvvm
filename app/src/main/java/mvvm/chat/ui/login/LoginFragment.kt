package mvvm.chat.ui.login

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import com.android.mvvm.R
import com.android.mvvm.databinding.FragmentLoginBinding
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginResult
import mvvm.chat.base.BaseFragment
import java.util.*


class LoginFragment : BaseFragment(){
    private lateinit var binding: FragmentLoginBinding
    private lateinit var viewModel: LoginViewModel

    private var fbCallbackManager: CallbackManager? = null


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentLoginBinding.inflate(inflater, container, false).apply {
            viewModel = ViewModelProviders.of(this@LoginFragment).get(LoginViewModel::class.java)
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupListener()
        setupObserver()
        changeColorButton()
        view.setOnTouchListener { _, _ ->
            hideSoftKeyboard(requireActivity())
            false
        }
        loginWithFacebook()
    }
    private fun setupListener(){
        binding.btnLogin.setOnClickListener{
            val email : String = binding.edtLoginEmail.text.toString().trim()
            val pass : String = binding.edtLoginPassword.text.toString().trim()
            viewModel.login(email, pass)
        }

        binding.txtRegister.setOnClickListener{
            NavHostFragment.findNavController(this).navigate(R.id.action_loginFragment_to_registerFragment, null)
        }

        binding.txtResetPassword.setOnClickListener{
            NavHostFragment.findNavController(this).navigate(R.id.action_loginFragment_to_resetPasswordFragment, null)
        }
    }
    private fun setupObserver(){
        viewModel.resultData.observe(viewLifecycleOwner, Observer{
            it?.let {
                if (it == 1) {
                    NavHostFragment.findNavController(this).navigate(R.id.action_loginFragment_to_homeFragment, null)
                } else if (it == 2) {
                    Toast.makeText(activity, getString(R.string.login_field), Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    private fun changeColorButton() {
        binding.edtLoginEmail.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable) {
                if (binding.edtLoginEmail.text!!.isNotEmpty() && binding.edtLoginPassword.text!!.isNotEmpty()) {
                    binding.btnLogin.setBackgroundResource(R.drawable.custom_button_blue)
                } else {
                    binding.btnLogin.setBackgroundResource(R.drawable.custom_button_grey)
                }
            }
        })
        binding.edtLoginPassword.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable) {
                if (binding.edtLoginEmail.text!!.isNotEmpty() && binding.edtLoginPassword.text!!.isNotEmpty()) {
                    binding.btnLogin.setBackgroundResource(R.drawable.custom_button_blue)
                } else {
                    binding.btnLogin.setBackgroundResource(R.drawable.custom_button_grey)
                }
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        fbCallbackManager?.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun loginWithFacebook() {
        fbCallbackManager = CallbackManager.Factory.create()
        binding.btnLoginFacebook.setReadPermissions(listOf("email", "public_profile"))
        binding.btnLoginFacebook.fragment = this
        binding.btnLoginFacebook.registerCallback(
            fbCallbackManager,
            object : FacebookCallback<LoginResult> {
                override fun onSuccess(loginResult: LoginResult) {
                    Log.d("Facebook Login", "Login success. Result = $loginResult")
                    this@LoginFragment.activity?.let { viewModel.handleFacebookAccessToken(it, loginResult.accessToken) }
                }

                override fun onCancel() {
                    Log.d("Facebook Login", "Login cancelled")
                }

                override fun onError(error: FacebookException) {
                    Log.d("Facebook Login", "Login failed. Result = $error")
                }
            })
    }

}