package mvvm.chat.ui.userprofile

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.view.*
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import com.android.mvvm.R
import com.android.mvvm.databinding.FragmentUserProfileBinding
import com.bumptech.glide.Glide
import mvvm.chat.base.BaseFragment
import mvvm.chat.data.Users
import mvvm.chat.utils.Constants
import java.util.*


class UserProfileFragment : BaseFragment() {

    private lateinit var viewModel: UserProfileViewModel
    private lateinit var binding: FragmentUserProfileBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentUserProfileBinding.inflate(inflater, container, false).apply {
            viewModel = ViewModelProviders.of(this@UserProfileFragment)
                .get(UserProfileViewModel::class.java)
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        setupObservers()
        setListeners()
        view.setOnTouchListener { _, _ ->
            hideSoftKeyboard(requireActivity())
            false
        }
    }

    private fun initView() {
        if (arguments != null && requireArguments().containsKey(Constants.Bundle.BUNDLE_KEY_USERS)) {
            val users = requireArguments().getSerializable(Constants.Bundle.BUNDLE_KEY_USERS) as Users?
            if (users != null) {
                binding.btnGoMess.setOnClickListener {
                    viewModel.checkExistRoom(users)
                }
                binding.btnAddFriend.setOnClickListener {
                    viewModel.sendRequest(users, requireContext())
                    binding.btnCancelFriend.isEnabled = true
                    binding.btnCancelFriend.visibility = View.VISIBLE
                    binding.btnAddFriend.visibility = View.GONE
                }
                binding.btnCancelFriend.setOnClickListener {
                    viewModel.cancelRequest(users)
                    binding.btnAddFriend.isEnabled = true
                    binding.btnAddFriend.visibility = View.VISIBLE
                    binding.btnCancelFriend.visibility = View.GONE
                }
                users.id?.let {
                    viewModel.showProfile(it)
                    viewModel.checkFriend(it)
                    viewModel.checkRequestFriend(it)
                }
            }
        }
    }

    private fun setListeners() {
        binding.imgBack.setOnClickListener {
            Navigation.findNavController(requireView()).navigateUp()
        }
    }

    @SuppressLint("ResourceAsColor")
    private fun setupObservers() {
        viewModel.resultUser.observe(viewLifecycleOwner) {
            it?.let {
                binding.txtNameProfile.text = if (it.username.equals("default"))getString(R.string.no_data) else it.username
                binding.name.text = if (it.username.equals("default"))getString(R.string.no_data) else it.username
                binding.phoneNumber.text = if (it.phoneNumber.equals("default"))getString(R.string.no_data) else it.phoneNumber
                binding.birthday.text = if (it.dateOfBirth.equals("default"))getString(R.string.no_data) else it.dateOfBirth
                binding.address.text = if (it.address.equals("default"))getString(R.string.no_data) else it.address
                if (context != null) {
                    if (it.imageURL.equals(Constants.Firebase.FIRE_BASE_DEFAULT)) {
                        Glide.with(requireContext()).load(R.drawable.ic_avt)
                            .into(binding.imgCircleProfile)
                    } else {
                        Glide.with(requireContext()).load(it.imageURL)
                            .into(binding.imgCircleProfile)
                    }
                }
            }
        }

        viewModel.haveFriend.observe(viewLifecycleOwner) {
            it?.let {
                if (it) {
                    binding.btnGoMess.run {
                        isEnabled = true
                        setBackgroundResource(R.drawable.custom_button_blue)
                        setTextColor(Color.WHITE)
                        setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_messenger_white, 0, 0, 0)
                    }
                    binding.btnAddFriend.run {
                        isEnabled = false
                        setBackgroundResource(R.drawable.custom_button_grey)
                        setTextColor(Color.BLACK)
                        setText(R.string.friend)
                        setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_add_friend, 0, 0, 0)
                    }
                } else {
                    binding.btnGoMess.run {
                        isEnabled = false
                        setBackgroundResource(R.drawable.custom_button_grey)
                        setTextColor(Color.BLACK)
                        setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_messenger_black, 0, 0, 0)
                    }
                    binding.btnAddFriend.run {
                        isEnabled = true
                        setBackgroundResource(R.drawable.custom_button_blue)
                        setTextColor(Color.WHITE)
                        setText(R.string.add_friend)
                        setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_add_friend_white, 0, 0, 0)
                    }
                }
            }
        }

        viewModel.haveRq.observe(viewLifecycleOwner) {
            it?.let {
                if (it) {
                    binding.btnCancelFriend.visibility = View.VISIBLE
                    binding.btnAddFriend.visibility = View.GONE

                } else {
                    binding.btnAddFriend.visibility = View.VISIBLE
                    binding.btnCancelFriend.visibility = View.GONE
                }
            }
        }

        viewModel.openChatRoom.observe(viewLifecycleOwner) {
            it?.let {
                val bundle = Bundle()
                bundle.putSerializable(Constants.Bundle.BUNDLE_KEY_ROOM, it)
                NavHostFragment.findNavController(this@UserProfileFragment)
                    .navigate(R.id.action_userProfile_to_messengerFragment, bundle)
            }
        }
    }
}