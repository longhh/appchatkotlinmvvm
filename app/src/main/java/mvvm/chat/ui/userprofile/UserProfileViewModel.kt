package mvvm.chat.ui.userprofile

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.android.mvvm.R
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.*
import com.google.firebase.ktx.Firebase
import mvvm.chat.base.BaseViewModel
import mvvm.chat.data.Room
import mvvm.chat.data.Users
import mvvm.chat.fcm.*
import mvvm.chat.model.RepoRepository
import mvvm.chat.model.api.APIFirebaseService
import mvvm.chat.utils.Constants
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class UserProfileViewModel : BaseViewModel() {
    val resultUser = MutableLiveData<Users>()
    val haveFriend = MutableLiveData<Boolean>()
    val haveRq = MutableLiveData<Boolean>(false)
    val openChatRoom = MutableLiveData<Room>()
    private var sender: Users? = null
    var apiService: APIFirebaseService = Client.getClient("https://fcm.googleapis.com/")!!.create(
        APIFirebaseService::class.java
    )

    init {
        getDataUser()
    }

    private fun getDataUser() {
        val userCurrent = Firebase.auth.currentUser
        userCurrent?.let {
            FirebaseDatabase.getInstance().getReference(Constants.Reference.REFERENCE_USERS)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        val user = Firebase.auth.currentUser
                        user?.let {
                            FirebaseDatabase.getInstance().getReference(Constants.Reference.REFERENCE_USERS)
                                .child(it.uid)
                                .addValueEventListener(object : ValueEventListener {
                                    override fun onDataChange(snapshot: DataSnapshot) {
                                        sender = snapshot.getValue(Users::class.java)
                                    }
                                    override fun onCancelled(error: DatabaseError) {
                                    }
                                })
                        }
                    }
                    override fun onCancelled(error: DatabaseError) {
                    }

                })
        }
    }

    fun showProfile(userId: String) {
        val user = Firebase.auth.currentUser
        user?.let {
            FirebaseDatabase.getInstance().getReference(Constants.Firebase.FIRE_BASE_USERS)
                .child(userId)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        val users: Users? = dataSnapshot.getValue(Users::class.java)
                        if (users != null) {
                            resultUser.postValue(users)
                        }
                    }

                    override fun onCancelled(databaseError: DatabaseError) {}
                })
        }
    }

    fun checkFriend(userId: String) {
        val userCurrent = Firebase.auth.currentUser
        userCurrent?.let {
            val uid: String = it.uid
            FirebaseDatabase.getInstance().getReference(Constants.Reference.REFERENCE_FRIENDS)
                .child(uid)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        for (snapshot in dataSnapshot.children) {
                            val users: Users? = snapshot.getValue(Users::class.java)
                            if (users != null) {
                                if (users.id.equals(userId)) haveFriend.postValue(true)
                                else haveFriend.postValue(false)
                            }
                        }
                    }

                    override fun onCancelled(databaseError: DatabaseError) {}
                })
        }
    }

    fun checkExistRoom(user: Users){
        val userCurrent = Firebase.auth.currentUser
        userCurrent?.let {
            val uid: String = it.uid
            FirebaseDatabase.getInstance().getReference(Constants.Reference.REFERENCE_ROOMS)
                .child(uid).get()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful && task.result != null) {
                        for (snapshot in task.result.children) {
                            val room = snapshot.getValue(Room::class.java)
                            if (room?.receiver != null && room.receiver.equals(user.id)) {
                                openChatRoom.postValue(room)
                                return@addOnCompleteListener
                            }
                        }
                        createChatRoom(user)
                    }
                }
        }
    }

    private fun createChatRoom(user: Users) {
        val roomKey: String? = RepoRepository.getInstance().generateRoomKey()
        val newRoom = Room(
            roomKey!!, user.username!!,
            user.imageURL!!, user.id!!, 0, "", getTimestamp(), user.username!!.toLowerCase(Locale.ROOT), "false", "noOne"
        )
        val childUpdates: HashMap<String, Any> = HashMap()
        val userCurrent = Firebase.auth.currentUser
        userCurrent?.let {
            childUpdates.put(it.uid + "/" + newRoom.id, newRoom.toMap())
        }
        childUpdates[user.id.toString() + "/" + newRoom.id] = createReceiverRoom(newRoom).toMap()
        FirebaseDatabase.getInstance()
            .getReference(Constants.Reference.REFERENCE_ROOMS)
            .updateChildren(childUpdates)
            .addOnSuccessListener{ openChatRoom.postValue(newRoom) }
    }

    private fun getTimestamp(): String {
        val tsLong = System.currentTimeMillis()
        return tsLong.toString()
    }

    private fun createReceiverRoom(newRoom: Room): Room {
        return Room(
            newRoom.id!!,
            sender?.username!!,
            sender?.imageURL!!,
            sender?.id!!,
            newRoom.unreadMessage,
            newRoom.lastMessage,
            newRoom.timestamp!!,
            newRoom.searchByName!!,
            "false",
            "noOne"
        )
    }

    fun sendRequest(user: Users, context: Context) {
        val users = Firebase.auth.currentUser
        users?.let {
            FirebaseDatabase.getInstance().reference
                .child(Constants.Reference.REFERENCE_FRIEND_REQUEST)
                .child(it.uid)
                .child(Constants.Reference.REFERENCE_REQUEST_SEND)
                .push()
                .setValue(user)
            user.let { it1 ->
                FirebaseDatabase.getInstance().reference
                    .child(Constants.Reference.REFERENCE_FRIEND_REQUEST)
                    .child(it1.id.toString())
                    .child(Constants.Reference.REFERENCE_REQUEST_RECEIVE)
                    .push()
                    .setValue(sender)
                sendNotification(it1, it.uid, context)
            }
        }
    }

    fun cancelRequest(user: Users) {
        val users = Firebase.auth.currentUser
        users?.let {
            removeValueSender(
                user,
                it.uid,
                Constants.Reference.REFERENCE_REQUEST_SEND
            )
            user.id?.let { it1 ->
                removeValueSender(
                    sender!!,
                    it1, Constants.Reference.REFERENCE_REQUEST_RECEIVE
                )
            }
        }
    }

    private fun removeValueSender(users: Users, id: String, table: String) {
        val query: Query = FirebaseDatabase.getInstance().reference
            .child(Constants.Reference.REFERENCE_FRIEND_REQUEST)
            .child(id)
            .child(table)
            .orderByChild(Constants.Firebase.FIRE_BASE_ID).equalTo(users.id)
        query.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (appleSnapshot in dataSnapshot.children) {
                    appleSnapshot.ref.removeValue()
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {}
        })
    }

    private fun sendNotification(users: Users, uid: String, context: Context) {
        FirebaseDatabase.getInstance()
            .getReference(Constants.Reference.REFERENCE_TOKENS)
            .orderByKey()
            .equalTo(users.id)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    for (dataSnapshot in snapshot.children) {
                        val token = dataSnapshot.getValue(Token::class.java)
                        val data = Data(
                            uid,
                            R.mipmap.ic_launcher,
                            sender!!.username.toString() + " " + context.getString(R.string.sent_add_friend),
                            context.getString(R.string.text_noti),
                            users.id!!
                        )
                        assert(token != null)
                        val sender = Sender(data, token!!.token)
                        apiService.sendNotifications(sender)
                            ?.enqueue(object : Callback<MyResponse?> {
                                override fun onResponse(
                                    call: Call<MyResponse?>,
                                    response: Response<MyResponse?>
                                ) {
                                    if (response.code() == 200) {
                                        assert(response.body() != null)
                                    }
                                }

                                override fun onFailure(call: Call<MyResponse?>, t: Throwable) {}
                            })
                    }
                }

                override fun onCancelled(error: DatabaseError) {}
            })
    }

    fun checkRequestFriend(userId: String) {
        val user = Firebase.auth.currentUser
        user?.let {
            FirebaseDatabase.getInstance().getReference(Constants.Reference.REFERENCE_FRIEND_REQUEST)
                .child(it.uid)
                .child(Constants.Reference.REFERENCE_REQUEST_SEND)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        for (snapshot in dataSnapshot.children) {
                            val users: Users? = snapshot.getValue(Users::class.java)
                            if (users != null) {
                                if (users.id.equals(userId)) haveRq.postValue(true)
                                else haveRq.postValue(false)
                            }
                        }
                    }
                    override fun onCancelled(error: DatabaseError) {

                    }
                })
        }
    }
}