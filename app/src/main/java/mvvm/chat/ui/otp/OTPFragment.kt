package mvvm.chat.ui.otp

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.android.mvvm.databinding.FragmentOtpBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthProvider
import mvvm.chat.base.BaseFragment
import mvvm.chat.utils.Constants

class OTPFragment : BaseFragment() {

    private lateinit var viewModel: OTPViewModel
    private lateinit var binding: FragmentOtpBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentOtpBinding.inflate(inflater, container, false).apply {
            viewModel = ViewModelProviders.of(this@OTPFragment)
                .get(OTPViewModel::class.java)
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupObservers()
        setListeners()
        view.setOnTouchListener { _, _ ->
            hideSoftKeyboard(requireActivity())
            false
        }
    }

    private fun setListeners() {
        if (arguments != null) {
            val phoneNumber = requireArguments().getSerializable(Constants.Bundle.BUNDLE_PHONE_NUMBER) as String?
            val verificationId = requireArguments().getSerializable(Constants.Bundle.BUNDLE_VERIFY) as String?
            if (verificationId != null && phoneNumber != null) {
                binding.tvMobile.text = String.format("+84-%s", phoneNumber)


                binding.tvResendBtn.setOnClickListener {
                    Toast.makeText(requireContext(), "OTP Send Successfully.", Toast.LENGTH_SHORT).show()
                }

                binding.btnVerify.setOnClickListener {
                    binding.progressBarVerify.visibility = View.VISIBLE
                    binding.btnVerify.visibility = View.INVISIBLE
                    if (binding.etC1.text.toString().trim().isEmpty() ||
                        binding.etC2.text.toString().trim().isEmpty() ||
                        binding.etC3.text.toString().trim().isEmpty() ||
                        binding.etC4.text.toString().trim().isEmpty() ||
                        binding.etC5.text.toString().trim().isEmpty() ||
                        binding.etC6.text.toString().trim().isEmpty()
                    ) {
                        Toast.makeText(requireContext(), "OTP is not Valid!", Toast.LENGTH_SHORT).show()
                    } else {
                        val code = binding.etC1.text.toString().trim() +
                                binding.etC2.text.toString().trim() +
                                binding.etC3.text.toString().trim() +
                                binding.etC4.text.toString().trim() +
                                binding.etC5.text.toString().trim() +
                                binding.etC6.text.toString().trim()
                        val credential = PhoneAuthProvider.getCredential(verificationId, code)
                        FirebaseAuth.getInstance()
                            .signInWithCredential(credential)
                            .addOnCompleteListener { task ->
                                if (task.isSuccessful) {
                                    binding.progressBarVerify.visibility = View.VISIBLE
                                    binding.btnVerify.visibility = View.INVISIBLE
                                    Toast.makeText(requireContext(), "Welcome...", Toast.LENGTH_SHORT).show()
                                    Log.d("abcd", "ok")
                                } else {
                                    binding.progressBarVerify.visibility = View.GONE
                                    binding.btnVerify.visibility = View.VISIBLE
                                    Toast.makeText(requireContext(), "OTP is not Valid!", Toast.LENGTH_SHORT).show()
                                }
                            }
                    }
                }
            }
        }
    }

    private fun setupObservers() {

    }
}