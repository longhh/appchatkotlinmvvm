package mvvm.chat.ui.sendvideo

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import com.android.mvvm.R
import com.android.mvvm.databinding.FragmentSendVideoBinding
import com.bumptech.glide.Glide
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.fragment_send_video.view.*
import mvvm.chat.base.BaseFragment
import mvvm.chat.data.Room
import mvvm.chat.utils.Constants
import org.jitsi.meet.sdk.BroadcastEvent
import org.jitsi.meet.sdk.JitsiMeet
import org.jitsi.meet.sdk.JitsiMeetActivity
import org.jitsi.meet.sdk.JitsiMeetConferenceOptions
import timber.log.Timber
import java.net.MalformedURLException
import java.net.URL
import java.util.HashMap


class SendVideoFragment : BaseFragment() {

    private val broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            onBroadcastReceived(intent)
        }
    }

    private lateinit var binding: FragmentSendVideoBinding
    private lateinit var viewModel: SendVideoViewModel
    private var roomId: String? = null
    private var receiver: String? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSendVideoBinding.inflate(inflater, container, false).apply {
            viewModel =
                ViewModelProviders.of(this@SendVideoFragment).get(SendVideoViewModel::class.java)
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        view.setOnTouchListener { _, _ ->
            hideSoftKeyboard(requireActivity())
            false
        }
        val serverURL: URL = try {
            URL("https://meet.jit.si")
        } catch (e: MalformedURLException) {
            e.printStackTrace()
            throw RuntimeException("Invalid server URL!")
        }
        val defaultOptions = JitsiMeetConferenceOptions.Builder()
            .setServerURL(serverURL)
            .setFeatureFlag("welcomepage.enabled", false)
            .build()
        JitsiMeet.setDefaultConferenceOptions(defaultOptions)

        registerForBroadcastMessages()
    }

    private fun initView() {
        if (arguments != null && requireArguments().containsKey(Constants.Bundle.BUNDLE_KEY_ROOM)) {
            val room = requireArguments().getSerializable(Constants.Bundle.BUNDLE_KEY_ROOM) as Room?
            if (room != null) {
                if (context != null) {
                    Glide.with(requireContext()).load(
                            if (room.thumbnail.equals(Constants.Firebase.FIRE_BASE_DEFAULT))
                                R.drawable.ic_avt
                            else
                                room.thumbnail
                        ).into(binding.imgCircleSendVideo)
                    binding.txtName.text = room.name
                    Glide.with(requireContext()).load(
                        if (room.thumbnail.equals(Constants.Firebase.FIRE_BASE_DEFAULT))
                            R.drawable.ic_avt
                        else
                            room.thumbnail
                    ).into(binding.backgroundSendVideo)
                }
                val map = HashMap<String, Any>()
                map[Constants.Firebase.CALLING] = "false"

                val user = Firebase.auth.currentUser
                user?.let {
                    receiver = room.receiver
                    val uid = user.uid
                    if (receiver != null) {
                        FirebaseDatabase.getInstance()
                            .getReference(Constants.Reference.REFERENCE_ROOMS)
                            .child(receiver!!)
                            .orderByChild(uid)
                            .addValueEventListener(object : ValueEventListener {
                                override fun onDataChange(snapshot: DataSnapshot) {
                                    for (snap in snapshot.children) {
                                        val rooms = snap.getValue(Room::class.java)
                                        if (rooms != null) {
                                            roomId = rooms.id
                                        }
                                    }
                                }

                                override fun onCancelled(error: DatabaseError) {

                                }
                            })

                    }
                    binding.btnCancelCalling.setOnClickListener {
                        Navigation.findNavController(requireView()).navigateUp()
                        roomId?.let { it1 ->
                            FirebaseDatabase.getInstance()
                                .getReference(Constants.Reference.REFERENCE_ROOMS)
                                .child(receiver!!)
                                .child(it1)
                                .updateChildren(map)

                            FirebaseDatabase.getInstance()
                                .getReference(Constants.Reference.REFERENCE_ROOMS)
                                .child(uid)
                                .child(it1)
                                .updateChildren(map)
                        }
                    }

                    FirebaseDatabase.getInstance().reference.child(Constants.Reference.REFERENCE_ROOMS)
                        .child(user.uid)
                        .addValueEventListener(object : ValueEventListener {
                            override fun onDataChange(snapshot: DataSnapshot) {
                                for (dataSnapshot in snapshot.children) {
                                    val rooms: Room? = dataSnapshot.getValue(Room::class.java)
                                    if (rooms != null) {
                                        if (room.calling == room.id) {
                                            val options = JitsiMeetConferenceOptions.Builder()
                                                .setRoom(roomId.toString())
                                                .build()
                                            JitsiMeetActivity.launch(requireContext(), options)
                                        }
                                    }
                                }
                            }
                            override fun onCancelled(error: DatabaseError) {
                            }
                        })
                }
            }
        }

    }

    private fun registerForBroadcastMessages() {
        val intentFilter = IntentFilter()
        for (type in BroadcastEvent.Type.values()) {
            intentFilter.addAction(type.action)
        }
        LocalBroadcastManager.getInstance(requireContext()).registerReceiver(broadcastReceiver, intentFilter)
    }

    private fun onBroadcastReceived(intent: Intent?) {
        if (intent != null) {
            val event = BroadcastEvent(intent)
            when (event.type) {
                BroadcastEvent.Type.CONFERENCE_JOINED -> Timber.i(
                    "Conference Joined with url%s",
                    event.data["url"]
                )
                BroadcastEvent.Type.PARTICIPANT_JOINED -> Timber.i(
                    "Participant joined%s",
                    event.data["name"]
                )
            }
        }
    }
}