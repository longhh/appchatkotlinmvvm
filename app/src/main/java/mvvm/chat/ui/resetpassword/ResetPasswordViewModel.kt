package mvvm.chat.ui.resetpassword

import androidx.lifecycle.MutableLiveData
import mvvm.chat.base.BaseViewModel
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import java.util.*

class ResetPasswordViewModel : BaseViewModel() {
    val result = MutableLiveData<Boolean>()

    fun resetPassword(email: String) {
        Firebase.auth.sendPasswordResetEmail("hailong741258@gmail.com")
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    result.postValue(true)
                } else {
                    result.postValue(false)
                }
            }
    }

}