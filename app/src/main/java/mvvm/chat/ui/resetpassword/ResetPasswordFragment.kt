package mvvm.chat.ui.resetpassword

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import com.android.mvvm.R
import com.android.mvvm.databinding.FragmentRegisterBinding
import androidx.navigation.Navigation
import com.android.mvvm.databinding.FragmentResetPasswordBinding
import mvvm.chat.base.BaseFragment


class ResetPasswordFragment : BaseFragment() {
    private lateinit var binding: FragmentResetPasswordBinding
    private lateinit var viewModel: ResetPasswordViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentResetPasswordBinding.inflate(inflater, container, false).apply {
            viewModel =
                ViewModelProviders.of(this@ResetPasswordFragment).get(ResetPasswordViewModel::class.java)
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupListener()
        setupObserver()
        changeColorButton()
        view.setOnTouchListener { _, _ ->
            hideSoftKeyboard(requireActivity())
            false
        }
    }

    private fun setupListener() {
        binding.btnConfirm.setOnClickListener {
            viewModel.resetPassword(binding.edtEmail.text.toString().trim())
        }

        binding.imgBack.setOnClickListener {
            Navigation.findNavController(requireView()).navigateUp()
        }

        binding.txtBack.setOnClickListener {
            Navigation.findNavController(requireView()).navigateUp()
        }
    }

    private fun setupObserver() {
        viewModel.result.observe(viewLifecycleOwner) {
            if (it) {
                Toast.makeText(requireContext(), "Email sent", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun changeColorButton() {
        binding.edtEmail.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable) {
                if (binding.edtEmail.text!!.isNotEmpty()) {
                    binding.btnConfirm.setBackgroundResource(R.drawable.custom_button_blue)
                } else {
                    binding.btnConfirm.setBackgroundResource(R.drawable.custom_button_grey)
                }
            }
        })
    }
}