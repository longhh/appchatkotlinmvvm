package mvvm.chat.ui.splash

import androidx.lifecycle.MutableLiveData
import mvvm.chat.base.BaseViewModel
import com.google.firebase.auth.FirebaseAuth

class SplashViewModel : BaseViewModel() {
    val checkUser = MutableLiveData<Int>()
    fun decideNextScreen(){
        if(FirebaseAuth.getInstance().currentUser == null){
            checkUser.postValue(OPEN_LOGIN)
        }else{
            checkUser.postValue(OPEN_HOME)
        }
    }

    companion object {
        const val OPEN_LOGIN: Int = 1
        const val OPEN_HOME: Int = 2
    }
}