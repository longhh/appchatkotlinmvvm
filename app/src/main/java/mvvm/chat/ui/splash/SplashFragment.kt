package mvvm.chat.ui.splash

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import com.android.mvvm.R
import com.android.mvvm.databinding.FragmentSplashBinding

class SplashFragment : Fragment() {

    private lateinit var viewModel: SplashViewModel
    private lateinit var binding: FragmentSplashBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSplashBinding.inflate(inflater, container, false).apply {
            viewModel = ViewModelProviders.of(this@SplashFragment).get(SplashViewModel::class.java)
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupObservers()
    }

    private fun setupObservers() {
        Handler(Looper.getMainLooper()).postDelayed({ viewModel.decideNextScreen() }, 1000)

        viewModel.checkUser.observe(viewLifecycleOwner) {
            if (it == OPEN_LOGIN) {
                NavHostFragment.findNavController(this).navigate(R.id.action_splashFragment_to_loginFragment, null)
            } else if (it == OPEN_HOME) {
                NavHostFragment.findNavController(this).navigate(R.id.action_splashFragment_to_homeFragment, null)
            }
        }
    }

    companion object {
        const val OPEN_LOGIN: Int = 1
        const val OPEN_HOME: Int = 2
    }
}