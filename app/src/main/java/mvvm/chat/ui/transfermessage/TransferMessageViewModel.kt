package mvvm.chat.ui.transfermessage

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.lifecycle.MutableLiveData
import com.android.mvvm.R
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.ktx.Firebase
import mvvm.chat.base.BaseViewModel
import mvvm.chat.data.FriendSort
import mvvm.chat.data.Message
import mvvm.chat.data.Room
import mvvm.chat.data.Users
import mvvm.chat.fcm.*
import mvvm.chat.model.api.APIFirebaseService
import mvvm.chat.utils.Constants
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class TransferMessageViewModel : BaseViewModel() {
    val resultFriendSort = MutableLiveData<List<FriendSort>>()

    private var userArrayList = ArrayList<Users>()
    private var users = ArrayList<Users>()
    private var sender: Users? = null
    private var sortUsers: ArrayList<FriendSort> = ArrayList()
    val uid: String = Firebase.auth.currentUser!!.uid
    private var room: Room? = null

    var apiService: APIFirebaseService = Client.getClient("https://fcm.googleapis.com/")!!.create(
        APIFirebaseService::class.java
    )

    init {
        readUser()
    }

    fun getRoom(idReceiver: String) {
        FirebaseDatabase.getInstance()
            .getReference(Constants.Reference.REFERENCE_ROOMS)
            .child(uid)
            .addValueEventListener(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    for (snapshot in dataSnapshot.children) {
                        val dataRoom: Room? = snapshot.getValue(Room::class.java)
                        if (dataRoom != null) {
                            if (dataRoom.receiver.equals(idReceiver)) {
                                room = dataRoom
                            }
                        }
                    }
                }

                override fun onCancelled(databaseError: DatabaseError) {
                }
            })
        getDataSender()
    }

    private fun getDataSender() {
        FirebaseDatabase.getInstance()
            .getReference(Constants.Firebase.FIRE_BASE_USERS)
            .child(uid)
            .addValueEventListener(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    sender = dataSnapshot.getValue(Users::class.java)
                }

                override fun onCancelled(databaseError: DatabaseError) {
                }
            })
    }

    private fun readUser() {
        val userCurrent = Firebase.auth.currentUser
        userCurrent?.let {
            val uid: String = it.uid
            FirebaseDatabase.getInstance().getReference(Constants.Reference.REFERENCE_FRIENDS)
                .child(uid)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        userArrayList.clear()
                        for (snapshot in dataSnapshot.children) {
                            val user = snapshot.getValue(Users::class.java)
                            if (user != null) {
                                if (!user.id.equals(uid)) {
                                    userArrayList.add(user)
                                }
                            }
                        }
                        readUserFriend(userArrayList)
                    }

                    override fun onCancelled(databaseError: DatabaseError) {}
                })
        }
    }

    private fun readUserFriend(userArrayList: ArrayList<Users>) {
        FirebaseDatabase.getInstance().getReference(Constants.Reference.REFERENCE_USERS)
            .addValueEventListener(object : ValueEventListener {
                @RequiresApi(api = Build.VERSION_CODES.N)
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    users.clear()
                    for (snapshot in dataSnapshot.children) {
                        val user = snapshot.getValue(Users::class.java)
                        for (i in userArrayList.indices) {
                            if (user != null && user.id.equals(userArrayList[i].id)) {
                                users.add(user)
                            }
                        }
                    }
                    sortListUserWithAlphabet(users)
                }

                override fun onCancelled(databaseError: DatabaseError) {}
            })
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private fun sortListUserWithAlphabet(users: ArrayList<Users>) {
        val sortedUsers = users.sortedBy { it.username?.first()?.toUpperCase().toString() }
        this.users = ArrayList(sortedUsers)
        filterListUser()
    }

    private fun filterListUser() {
        sortUsers = ArrayList()
        var lastHeader = ""
        for (user in users) {
            val header: String =
                java.lang.String.valueOf(user.username!!.first()).toUpperCase(Locale.ROOT)
            if (header != lastHeader) {
                lastHeader = header
                sortUsers.add(FriendSort(header, null, true))
            }
            sortUsers.add(FriendSort(header, user, false))
        }
        resultFriendSort.postValue(sortUsers)
    }

    fun searchUser(word: String) {
        val userArrayList = ArrayList<Users?>()
        val userCurrent = Firebase.auth.currentUser
        userCurrent?.let {
            val uid: String = it.uid
            FirebaseDatabase.getInstance().getReference(Constants.Reference.REFERENCE_FRIENDS)
                .child(uid)
                .addValueEventListener(object : ValueEventListener {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    override fun onDataChange(snapshot: DataSnapshot) {
                        userArrayList.clear()
                        for (dataSnapshot in snapshot.children) {
                            val user = dataSnapshot.getValue(Users::class.java)!!
                            if (!user.id.equals(uid)) {
                                userArrayList.add(user)
                            }
                        }
                        readUserSearch(userArrayList, word)
                    }

                    override fun onCancelled(error: DatabaseError) {}
                })
        }
    }

    private fun readUserSearch(userArrayList: ArrayList<Users?>, word: String) {
        FirebaseDatabase.getInstance().getReference(Constants.Reference.REFERENCE_USERS)
            .orderByChild(Constants.Firebase.FIRE_BASE_SEARCH)
            .startAt(word).endAt(word + "\uf8ff")
            .addValueEventListener(object : ValueEventListener {
                @RequiresApi(api = Build.VERSION_CODES.N)
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    users.clear()
                    for (snapshot in dataSnapshot.children) {
                        val user = snapshot.getValue(Users::class.java)
                        for (i in userArrayList.indices) {
                            if (user != null && user.id.equals(userArrayList[i]!!.id)) {
                                users.add(user)
                            }
                        }
                    }
                    sortListUser(users)
                }

                override fun onCancelled(databaseError: DatabaseError) {}
            })
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private fun sortListUser(userArrayList: ArrayList<Users>) {
        val sortedUserSearch =
            userArrayList.sortedBy { it.username?.first()?.toUpperCase().toString() }
        this.userArrayList = ArrayList(sortedUserSearch)
        filterListUserSearch(userArrayList)
    }

    private fun filterListUserSearch(userArrayList: ArrayList<Users>) {
        val usersSortSearch: ArrayList<FriendSort> = ArrayList()
        var lastHeader = ""
        for (user in userArrayList) {
            val header: String =
                java.lang.String.valueOf(user.username!!.first()).toUpperCase(Locale.ROOT)
            if (header != lastHeader) {
                lastHeader = header
                usersSortSearch.add(FriendSort(header, null, true))
            }
            usersSortSearch.add(FriendSort(header, user, false))
        }
        resultFriendSort.postValue(usersSortSearch)
    }

    fun sendMessage(message: String, context: Context?) {
        val newMessage = Message(uid, message, getCurrentTime())
        FirebaseDatabase.getInstance()
            .getReference(Constants.Reference.REFERENCE_MESSAGES)
            .child(room!!.id!!)
            .push()
            .setValue(newMessage)
            .addOnSuccessListener { updateChatRoom(message) }
        sendNotification(message, context)
    }

    @SuppressLint("SimpleDateFormat")
    private fun getCurrentTime(): String? {
        val date = Calendar.getInstance().time
        val df = SimpleDateFormat("dd-MM-yyyy HH:mm:ss")
        return df.format(date)
    }

    private fun sendNotification(message: String, context: Context?) {
        FirebaseDatabase.getInstance()
            .getReference(Constants.Reference.REFERENCE_TOKENS)
            .orderByKey()
            .equalTo(room!!.receiver)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    for (dataSnapshot in snapshot.children) {
                        val token = dataSnapshot.getValue(Token::class.java)
                        val data = Data(
                            uid,
                            R.mipmap.ic_launcher,
                            sender!!.username.toString() + " : " + message,
                            context!!.getString(R.string.new_message),
                            room!!.receiver!!
                        )
                        assert(token != null)
                        val sender = Sender(data, token!!.token)
                        apiService.sendNotifications(sender)
                            ?.enqueue(object : Callback<MyResponse?> {
                                override fun onResponse(
                                    call: Call<MyResponse?>,
                                    response: Response<MyResponse?>
                                ) {
                                    if (response.code() == 200) {
                                        assert(response.body() != null)
                                    }
                                }

                                override fun onFailure(call: Call<MyResponse?>, t: Throwable) {}
                            })
                    }
                }

                override fun onCancelled(error: DatabaseError) {}
            })
    }

    private fun updateChatRoom(message: String) {
        room!!.lastMessage = message
        room!!.timestamp = getTimestamp()
        //Update chat room sender
        FirebaseDatabase.getInstance()
            .getReference(Constants.Reference.REFERENCE_ROOMS)
            .child(uid)
            .child(room!!.id!!)
            .setValue(room)

        //Update chat room receiver
        FirebaseDatabase.getInstance()
            .getReference(Constants.Reference.REFERENCE_ROOMS)
            .child(room!!.receiver!!)
            .child(room!!.id!!)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    val room = dataSnapshot.getValue(Room::class.java)
                    if (room != null) {
                        room.lastMessage = message
                        val unread: Int = room.unreadMessage + 1
                        room.name = sender!!.username
                        room.thumbnail = sender!!.imageURL
                        room.searchByName = sender!!.username!!.toLowerCase()
                        room.unreadMessage = unread
                        room.timestamp = getTimestamp()
                        dataSnapshot.ref.setValue(room)
                    }
                }

                override fun onCancelled(databaseError: DatabaseError) {
                }
            })

    }

    private fun updateChatRoom(sender: String, receiver: String) {
        room!!.lastMessage = sender
        room!!.timestamp = getTimestamp()

        //Update chat room sender
        FirebaseDatabase.getInstance()
            .getReference(Constants.Reference.REFERENCE_ROOMS)
            .child(uid)
            .child(room!!.id!!)
            .setValue(room)

        //Update chat room receiver
        FirebaseDatabase.getInstance()
            .getReference(Constants.Reference.REFERENCE_ROOMS)
            .child(room!!.receiver!!)
            .child(room!!.id!!)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    val room = dataSnapshot.getValue(Room::class.java)
                    if (room != null) {
                        room.lastMessage = receiver
                        val unread: Int = room.unreadMessage + 1
                        room.unreadMessage = unread
                        room.timestamp = getTimestamp()
                        dataSnapshot.ref.setValue(room)
                    }
                }

                override fun onCancelled(databaseError: DatabaseError) {
                }
            })
    }

    private fun getTimestamp(): String {
        val tsLong = System.currentTimeMillis()
        return tsLong.toString()
    }
}