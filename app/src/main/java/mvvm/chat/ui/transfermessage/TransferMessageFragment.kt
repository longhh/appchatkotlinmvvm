package mvvm.chat.ui.transfermessage

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import com.android.mvvm.databinding.FragmentTransferMessageBinding
import mvvm.chat.base.BaseFragment
import mvvm.chat.utils.Constants
import java.util.*

class TransferMessageFragment : BaseFragment() {

    private lateinit var viewModel: TransferMessageViewModel
    private lateinit var binding: FragmentTransferMessageBinding

    private var adapter: TransferMessageAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTransferMessageBinding.inflate(inflater, container, false).apply {
            viewModel =
                ViewModelProviders.of(this@TransferMessageFragment)
                    .get(TransferMessageViewModel::class.java)
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initData()
        setInitAdapter()
        setListeners()
        view.setOnTouchListener { _, _ ->
            hideSoftKeyboard(requireActivity())
            false
        }
    }

    private fun initData() {
        val content = requireArguments().getSerializable(Constants.Bundle.BUNDLE_CONTENT) as String?
        viewModel.resultFriendSort.observe(viewLifecycleOwner) {
            it?.let {
                if (adapter != null) {
                    adapter!!.setData(it)
                    adapter!!.setOnClick(object : TransferMessageAdapter.IOnClick {
                        override fun onItemCLick(i: Int) {
                            if (content != null) {
                                it[i].users?.id?.let { it1 -> viewModel.getRoom(it1) }
                                Handler().postDelayed({
                                    viewModel.sendMessage(content, context)
                                }, 1000)
                            }
                        }
                    })
                }
            }
        }

    }

    private fun setListeners() {
        binding.imgBack.setOnClickListener {
            Navigation.findNavController(requireView()).navigateUp()
        }
    }

    private fun setInitAdapter() {
        binding.recyclerViewTabFriend.setHasFixedSize(true)
        adapter = TransferMessageAdapter(requireContext(), ArrayList())
        binding.recyclerViewTabFriend.adapter = adapter
    }
}