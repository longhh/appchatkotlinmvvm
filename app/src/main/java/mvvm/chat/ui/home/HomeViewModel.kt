package mvvm.chat.ui.home

import androidx.lifecycle.MutableLiveData
import mvvm.chat.base.BaseViewModel
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.*
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.FirebaseMessaging
import mvvm.chat.data.Room
import mvvm.chat.data.Users
import mvvm.chat.fcm.Token
import mvvm.chat.utils.Constants
import java.util.ArrayList

class HomeViewModel : BaseViewModel() {
    val countMess = MutableLiveData<Int>()
    val countFriend = MutableLiveData<Int>()

    init {
        updateToken()
    }

    fun getCountMess() {
        val user = Firebase.auth.currentUser
        user?.let {
            FirebaseDatabase.getInstance().getReference(Constants.Reference.REFERENCE_ROOMS)
                .child(it.uid)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        val rooms: ArrayList<Room> = ArrayList<Room>()
                        for (dataSnapshot in snapshot.children) {
                            val room: Room? = dataSnapshot.getValue(Room::class.java)
                            if (room != null && !room.lastMessage.equals("")) {
                                rooms.add(room)
                            }
                        }
                        var count = 0
                        for (i in rooms.indices) {
                            if (rooms[i].unreadMessage > 0) {
                                count++
                            }
                        }
                        countMess.postValue(count)
                    }

                    override fun onCancelled(error: DatabaseError) {

                    }
                })
        }
    }

    fun getCountFriend() {
        val userArrayList: ArrayList<Users> = ArrayList<Users>()
        val user = Firebase.auth.currentUser
        user?.let {
            FirebaseDatabase.getInstance()
                .getReference(Constants.Reference.REFERENCE_FRIEND_REQUEST).child(it.uid)
                .child(Constants.Reference.REFERENCE_REQUEST_RECEIVE)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        userArrayList.clear()
                        for (dataSnapshot in snapshot.children) {
                            val userSnapshot: Users? = dataSnapshot.getValue(Users::class.java)
                            if (userSnapshot != null) {
                                if (!userSnapshot.id?.equals(it)!!) {
                                    userArrayList.add(userSnapshot)
                                }
                            }
                        }
                        countFriend.postValue(userArrayList.size)
                    }

                    override fun onCancelled(error: DatabaseError) {

                    }
                })
        }
    }

    private fun updateToken() {
        val user = Firebase.auth.currentUser
        user?.let {
            FirebaseMessaging.getInstance().token
                .addOnCompleteListener(OnCompleteListener { task ->
                    if (!task.isSuccessful) {
                        return@OnCompleteListener
                    }
                    FirebaseDatabase.getInstance().getReference(Constants.Reference.REFERENCE_TOKENS)
                        .child(user.uid)
                        .setValue(Token(task.result))
                })
        }
    }
}