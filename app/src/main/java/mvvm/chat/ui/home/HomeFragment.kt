package mvvm.chat.ui.home

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.android.mvvm.R
import com.android.mvvm.databinding.FragmentHomeBinding
import com.google.android.material.badge.BadgeDrawable
import mvvm.chat.base.BaseFragment
import mvvm.chat.ui.homefriend.HomeFriendFragment
import mvvm.chat.ui.homemessenger.HomeMessengerFragment
import mvvm.chat.ui.homeprofile.HomeProfileFragment

class HomeFragment : BaseFragment() {

    private lateinit var viewModel: HomeViewModel
    private lateinit var binding: FragmentHomeBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeBinding.inflate(inflater, container, false).apply {
            viewModel = ViewModelProviders.of(this@HomeFragment).get(HomeViewModel::class.java)
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupObservers()
        setListeners()
        view.setOnTouchListener { _, _ ->
            hideSoftKeyboard(requireActivity())
            false
        }
    }

    private fun setListeners() {
        binding.bottomNavHome.setOnNavigationItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.action_messenger -> binding.viewPagerHome.currentItem = 0
                R.id.action_friend -> binding.viewPagerHome.currentItem = 1
                R.id.action_profile -> binding.viewPagerHome.currentItem = 2
            }
            true
        }
        val viewPagerAdapter = ViewPagerHomeAdapter(childFragmentManager)
        viewPagerAdapter.addFragment(HomeMessengerFragment())
        viewPagerAdapter.addFragment(HomeFriendFragment())
        viewPagerAdapter.addFragment(HomeProfileFragment())
        binding.viewPagerHome.adapter = viewPagerAdapter
        binding.viewPagerHome.offscreenPageLimit = viewPagerAdapter.count
        binding.viewPagerHome.addOnPageChangeListener(object : OnPageChangeListener {
            override fun onPageScrolled(i: Int, v: Float, i1: Int) {}
            override fun onPageSelected(i: Int) {
                when (i) {
                    0 -> binding.bottomNavHome.menu.findItem(R.id.action_messenger).isChecked = true
                    1 -> binding.bottomNavHome.menu.findItem(R.id.action_friend).isChecked = true
                    2 -> binding.bottomNavHome.menu.findItem(R.id.action_profile).isChecked = true
                }
            }

            override fun onPageScrollStateChanged(i: Int) {}
        })
    }

    private fun setupObservers() {
        viewModel.getCountMess()
        viewModel.countMess.observe(viewLifecycleOwner) {
            run {
                val badgeDrawable: BadgeDrawable =
                    binding.bottomNavHome.getOrCreateBadge(R.id.action_messenger)
                if (it > 0) {
                    badgeDrawable.isVisible = true
                    badgeDrawable.number = it
                } else {
                    badgeDrawable.isVisible = false
                }
            }
        }

        viewModel.getCountFriend()
        viewModel.countFriend.observe(viewLifecycleOwner) {
            run {
                val badgeDrawable1: BadgeDrawable =
                    binding.bottomNavHome.getOrCreateBadge(R.id.action_friend)
                if (it > 0) {
                    badgeDrawable1.isVisible = true
                    badgeDrawable1.number = it
                } else {
                    badgeDrawable1.isVisible = false
                }
            }
        }
    }
}