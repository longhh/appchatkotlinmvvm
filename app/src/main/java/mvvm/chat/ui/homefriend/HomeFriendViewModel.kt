package mvvm.chat.ui.homefriend

import androidx.lifecycle.MutableLiveData
import mvvm.chat.base.BaseViewModel
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.ktx.Firebase
import mvvm.chat.data.Users
import mvvm.chat.utils.Constants
import java.util.ArrayList

class HomeFriendViewModel : BaseViewModel() {
    val countFriend = MutableLiveData<Int>()

    init {
        getCountFriend()
    }

    private fun getCountFriend(){
        val userArrayList: ArrayList<Users> = ArrayList<Users>()
        val user = Firebase.auth.currentUser
        user?.let {
            FirebaseDatabase.getInstance().getReference(Constants.Reference.REFERENCE_FRIEND_REQUEST).child(it.uid)
                .child(Constants.Reference.REFERENCE_REQUEST_RECEIVE)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        userArrayList.clear()
                        for (dataSnapshot in snapshot.children) {
                            val users: Users? = dataSnapshot.getValue(Users::class.java)
                            if (users != null) {
                                if (!users.id?.equals(it)!!) {
                                    userArrayList.add(users)
                                }
                            }
                        }
                        countFriend.postValue(userArrayList.size)
                    }
                    override fun onCancelled(error: DatabaseError) {

                    }
                })
        }
    }
}