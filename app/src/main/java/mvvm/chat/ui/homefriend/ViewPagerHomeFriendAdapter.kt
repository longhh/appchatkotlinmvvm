package mvvm.chat.ui.homefriend

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import java.util.ArrayList

class ViewPagerHomeFriendAdapter(fragmentManager: FragmentManager) :
    FragmentPagerAdapter(fragmentManager) {
    private var fragmentArrayList: ArrayList<Fragment>? = null
    private var titleArrayList: ArrayList<String>? = null

    init {
        fragmentArrayList = ArrayList()
        titleArrayList = ArrayList()
    }

    override fun getItem(position: Int): Fragment {
        return fragmentArrayList!![position]
    }

    override fun getCount(): Int {
        return fragmentArrayList!!.size
    }

    fun addFragment(fragment: Fragment, title: String) {
        fragmentArrayList!!.add(fragment)
        titleArrayList!!.add(title)
    }

    override fun getPageTitle(position: Int): CharSequence {
        return titleArrayList!![position]
    }
}