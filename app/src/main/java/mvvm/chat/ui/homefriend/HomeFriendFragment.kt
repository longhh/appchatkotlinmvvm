package mvvm.chat.ui.homefriend

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.android.mvvm.R
import com.android.mvvm.databinding.FragmentHomeFriendBinding
import mvvm.chat.base.BaseFragment
import mvvm.chat.ui.tabfriend.TabFriendFragment
import mvvm.chat.ui.tabrequest.TabRequestFragment
import mvvm.chat.ui.tabuser.TabUserFragment
import java.util.*

class HomeFriendFragment : BaseFragment() {

    private lateinit var viewModel: HomeFriendViewModel
    private lateinit var binding: FragmentHomeFriendBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeFriendBinding.inflate(inflater, container, false).apply {
            viewModel =
                ViewModelProviders.of(this@HomeFriendFragment).get(HomeFriendViewModel::class.java)
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }

    private var onTextChanged: OnTextChanged? = null

    interface OnTextChanged {
        fun onTextChanged(word: String?)
    }

    fun setOnTextChanged(onTextChanged: OnTextChanged?) {
        this.onTextChanged = onTextChanged
    }

    private var onTextChanged2: OnTextChanged2? = null

    interface OnTextChanged2 {
        fun onTextChanged2(word: String?)
    }

    fun setOnTextChanged2(onTextChanged2: OnTextChanged2?) {
        this.onTextChanged2 = onTextChanged2
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
        setupObservers()
        view.setOnTouchListener { _, _ ->
            hideSoftKeyboard(requireActivity())
            false
        }
    }

    private fun setListeners() {
        binding.searchViewHomeFriend.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                if (binding.viewPagerFriend.currentItem == 0) {
                    if (onTextChanged != null) {
                        onTextChanged!!.onTextChanged(charSequence.toString().toLowerCase(Locale.getDefault()))
                    }
                } else if (binding.viewPagerFriend.currentItem == 1) {
                    if (onTextChanged2 != null) {
                        onTextChanged2!!.onTextChanged2(charSequence.toString().toLowerCase(Locale.getDefault()))
                    }
                }
            }

            override fun afterTextChanged(editable: Editable) {}
        })
        val adapter = ViewPagerHomeFriendAdapter(childFragmentManager)
        adapter.addFragment(TabFriendFragment(), requireContext().getString(R.string.tab_friend))
        adapter.addFragment(TabUserFragment(), requireContext().getString(R.string.tab_all))
        adapter.addFragment(TabRequestFragment(), requireContext().getString(R.string.tab_request))
        binding.viewPagerFriend.adapter = adapter
        binding.tabLayoutFriend.setupWithViewPager(binding.viewPagerFriend)
    }

    private fun setupObservers() {
        viewModel.countFriend.observe(viewLifecycleOwner) {
            it.let {
                if (it == 0) {
                    binding.txtRequestNumber.visibility = View.GONE
                } else {
                    binding.txtRequestNumber.visibility = View.VISIBLE
                    binding.txtRequestNumber.text = it.toString()
                }
            }
        }
    }
}