package mvvm.chat.ui.changeprofile

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import com.android.mvvm.R
import com.android.mvvm.databinding.DialogDatePickerBinding
import com.android.mvvm.databinding.DialogLoginAgainBinding
import com.android.mvvm.databinding.FragmentChangeProfileBinding
import com.bumptech.glide.Glide
import mvvm.chat.base.BaseFragment
import mvvm.chat.utils.Constants
import java.text.SimpleDateFormat
import java.util.*

class ChangeProfileFragment : BaseFragment() {

    private lateinit var viewModel: ChangeProfileViewModel
    private lateinit var binding: FragmentChangeProfileBinding
    private val imageRequest = 1
    private var mUri: Uri? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentChangeProfileBinding.inflate(inflater, container, false).apply {
            viewModel = ViewModelProviders.of(this@ChangeProfileFragment)
                .get(ChangeProfileViewModel::class.java)
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupObservers()
        setListeners()
        view.setOnTouchListener { _, _ ->
            hideSoftKeyboard(requireActivity())
            false
        }
    }

    private fun setListeners() {
        binding.imgBackHomeProfile.setOnClickListener {
            Navigation.findNavController(requireView()).navigateUp()
        }
        binding.imgCamera.setOnClickListener {
            val intent = Intent()
            intent.type = "image/*"
            intent.action = Intent.ACTION_GET_CONTENT
            startActivityForResult(intent, imageRequest)
        }
        binding.txtDone.setOnClickListener {
            if (mUri != null) {
                viewModel.uploadImage(mUri!!)
                Navigation.findNavController(requireView()).navigateUp()
            }
            if (binding.edtChangeName.text!!.isNotEmpty() && binding.edtChangeName.text.toString() != viewModel.resultUser.value!!.username) {
                viewModel.uploadName(binding.edtChangeName.text.toString().trim())
                Navigation.findNavController(requireView()).navigateUp()
            }
            if (binding.edtChangeAddress.text!!.isNotEmpty() && binding.edtChangeAddress.text.toString() != viewModel.resultUser.value!!.address) {
                viewModel.uploadAddress(binding.edtChangeAddress.text.toString().trim())
                Navigation.findNavController(requireView()).navigateUp()
            }
            if (binding.edtChangePhone.text!!.isNotEmpty() && binding.edtChangePhone.text.toString() != viewModel.resultUser.value!!.phoneNumber) {
                viewModel.uploadPhone(binding.edtChangePhone.text.toString().trim())
                Navigation.findNavController(requireView()).navigateUp()
            }
            if (binding.edtEmail.text!!.isNotEmpty() && binding.edtEmail.text.toString() != viewModel.resultEmail.value) {
                viewModel.uploadEmail(binding.edtEmail.text.toString().trim())
                showAlertDialogLogin()
                signOut()
            }
            if (binding.txtDateOfBirth.text!!.isNotEmpty() && binding.txtDateOfBirth.text.toString() != viewModel.resultUser.value!!.dateOfBirth) {
                viewModel.uploadDateOfBirth(binding.txtDateOfBirth.text.toString())
                Navigation.findNavController(requireView()).navigateUp()
            }
        }
        binding.txtDateOfBirth.setOnClickListener {
            showDatePicker()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun showDatePicker() {
        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val bindingDialog: DialogDatePickerBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context), R.layout.dialog_date_picker, null, false
        )
        dialog.setContentView(bindingDialog.root)
        val window = dialog.window
        window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val windowAttribute: WindowManager.LayoutParams = window?.attributes!!
        windowAttribute.gravity = Gravity.CENTER
        window.attributes = windowAttribute

        val today = Calendar.getInstance()
        bindingDialog.datePicker.init(today.get(Calendar.YEAR), today.get(Calendar.MONTH),
            today.get(Calendar.DAY_OF_MONTH)) { _, year, month, day ->
            val mMonth = month + 1
            val msg = "$day/$mMonth/$year"
            bindingDialog.btnConfirm.setOnClickListener {
                binding.txtDateOfBirth.text = msg
                dialog.dismiss()
            }
        }
        dialog.show()
    }

    private fun showAlertDialogLogin() {
        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

        val bindingDialog: DialogLoginAgainBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context), R.layout.dialog_login_again, null, false
        )
        dialog.setContentView(bindingDialog.root)
        val window = dialog.window
        window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val windowAttribute: WindowManager.LayoutParams = window?.attributes!!
        windowAttribute.gravity = Gravity.CENTER
        window.attributes = windowAttribute

        bindingDialog.btnConfirm.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun signOut() {
        viewModel.signOut()
        if (binding.edtChangeName.text.toString() != viewModel.resultUser.value!!.username) {
            NavHostFragment.findNavController(this)
                .navigate(R.id.action_homeFragment_to_loginFragment, null)
        } else {
            NavHostFragment.findNavController(this)
                .navigate(R.id.action_changeProfileFragment_to_loginFragment, null)
        }
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == imageRequest && resultCode == Activity.RESULT_OK && data != null && data.data != null) {
            mUri = data.data
            if (mUri != null) {
                if (context != null) {
                    if (mUri.toString() == Constants.Firebase.FIRE_BASE_DEFAULT)
                        Glide.with(requireContext()).load(R.drawable.ic_avt).into(binding.imgAva)
                    else
                        Glide.with(requireContext()).load(mUri.toString()).into(binding.imgAva)
                }
            }
        }
    }

    private fun setupObservers() {
        viewModel.resultUser.observe(viewLifecycleOwner) {
            it?.let {
                if (context != null) {
                    binding.edtChangeName.setText(it.username)
                    if (it.imageURL.equals(Constants.Firebase.FIRE_BASE_DEFAULT))
                        Glide.with(requireContext()).load(R.drawable.ic_avt).into(binding.imgAva)
                    else
                        Glide.with(requireContext()).load(it.imageURL).into(binding.imgAva)
                    if (!it.dateOfBirth.equals(Constants.Firebase.FIRE_BASE_DEFAULT)) {
                        binding.txtDateOfBirth.text = it.dateOfBirth
                    }
                    if (!it.address.equals(Constants.Firebase.FIRE_BASE_DEFAULT)) {
                        binding.edtChangeAddress.setText(it.address)
                    }
                    if (!it.phoneNumber.equals(Constants.Firebase.FIRE_BASE_DEFAULT)) {
                        binding.edtChangePhone.setText(it.phoneNumber)
                    }
                }
            }
        }
        viewModel.resultEmail.observe(viewLifecycleOwner) { email ->
            binding.edtEmail.setText(email.toString())
        }
    }
}