package mvvm.chat.ui.changeprofile

import android.net.Uri
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import mvvm.chat.base.BaseViewModel
import mvvm.chat.data.Users
import mvvm.chat.utils.Constants
import java.util.*

class ChangeProfileViewModel : BaseViewModel() {
    val resultUser = MutableLiveData<Users>()
    val resultEmail = MutableLiveData<String>()

    init {
        getDataUser()
    }

    private fun getDataUser() {
        val user = Firebase.auth.currentUser
        user?.let {
            resultEmail.postValue(user.email)
            val uid: String = user.uid
            FirebaseDatabase.getInstance().getReference(Constants.Firebase.FIRE_BASE_USERS)
                .child(uid)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        val users: Users? = dataSnapshot.getValue(Users::class.java)
                        resultUser.postValue(users)
                    }

                    override fun onCancelled(databaseError: DatabaseError) {}
                })
        }
    }

    fun uploadImage(mUri: Uri) {
        val reference = FirebaseStorage.getInstance().reference
        val riversRef = reference.child("images/" + mUri.lastPathSegment)
        riversRef.putFile(mUri).addOnSuccessListener {
            riversRef.downloadUrl.addOnSuccessListener { uri ->
                val user = Firebase.auth.currentUser
                user?.let {
                    val uid: String = user.uid
                    FirebaseDatabase.getInstance().getReference(Constants.Firebase.FIRE_BASE_USERS)
                        .child(uid)
                        .addListenerForSingleValueEvent(object : ValueEventListener {
                            override fun onDataChange(dataSnapshot: DataSnapshot) {
                                val users = dataSnapshot.getValue(Users::class.java)
                                if (users != null) {
                                    users.imageURL = uri.toString()
                                    dataSnapshot.ref.setValue(users)
                                }
                            }

                            override fun onCancelled(databaseError: DatabaseError) {}
                        })
                }
            }
        }
    }

    fun uploadName(name: String) {
        val map = HashMap<String, Any>()
        map[Constants.Firebase.FIRE_BASE_USERNAME] = name
        map[Constants.Firebase.FIRE_BASE_SEARCH] = name.toLowerCase()
        val user = Firebase.auth.currentUser
        user?.let {
            val uid: String = user.uid
            FirebaseDatabase.getInstance().getReference(Constants.Reference.REFERENCE_USERS)
                .child(uid)
                .updateChildren(map)
        }
    }

    fun uploadAddress(address: String) {
        val map = HashMap<String, Any>()
        map[Constants.Firebase.FIRE_BASE_ADDRESS] = address
        val user = Firebase.auth.currentUser
        user?.let {
            val uid: String = user.uid
            FirebaseDatabase.getInstance().getReference(Constants.Reference.REFERENCE_USERS)
                .child(uid)
                .updateChildren(map)
        }
    }


    fun uploadPhone(phone: String) {
        val map = HashMap<String, Any>()
        map[Constants.Firebase.FIRE_BASE_PHONE_NUMBER] = phone
        val user = Firebase.auth.currentUser
        user?.let {
            val uid: String = user.uid
            FirebaseDatabase.getInstance().getReference(Constants.Reference.REFERENCE_USERS)
                .child(uid)
                .updateChildren(map)
        }
    }

    fun uploadDateOfBirth(dateOfBirth: String) {
        val map = HashMap<String, Any>()
        map[Constants.Firebase.FIRE_BASE_DATE_OF_BIRTH] = dateOfBirth
        val user = Firebase.auth.currentUser
        user?.let {
            val uid: String = user.uid
            FirebaseDatabase.getInstance().getReference(Constants.Reference.REFERENCE_USERS)
                .child(uid)
                .updateChildren(map)
        }
    }

    fun uploadEmail(newEmail: String) {
        val map = HashMap<String, Any>()
        map[Constants.Firebase.FIRE_BASE_EMAIL] = newEmail
        val user = Firebase.auth.currentUser
        user!!.updateEmail(newEmail)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val uid: String = user.uid
                    FirebaseDatabase.getInstance().getReference(Constants.Reference.REFERENCE_USERS)
                        .child(uid)
                        .updateChildren(map)
                    Log.d("abcd", "User email address updated.")
                }
            }
    }

    fun signOut() {
        FirebaseAuth.getInstance().signOut()
    }
}