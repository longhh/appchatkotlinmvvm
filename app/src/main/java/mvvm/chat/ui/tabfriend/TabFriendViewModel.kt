package mvvm.chat.ui.tabfriend

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.lifecycle.MutableLiveData
import mvvm.chat.base.BaseViewModel
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.*
import com.google.firebase.ktx.Firebase
import mvvm.chat.data.FriendSort
import mvvm.chat.data.Room
import mvvm.chat.data.Users
import mvvm.chat.model.RepoRepository
import mvvm.chat.utils.Constants
import java.util.*
import kotlin.collections.ArrayList

class TabFriendViewModel : BaseViewModel() {
    val resultFriendSort = MutableLiveData<List<FriendSort>>()

    private var userArrayList = ArrayList<Users>()
    private var users = ArrayList<Users>()
    private var sortUsers: ArrayList<FriendSort> = ArrayList()

    init {
        readUser()
    }

    private fun readUser() {
        val userCurrent = Firebase.auth.currentUser
        userCurrent?.let {
            val uid: String = it.uid
            FirebaseDatabase.getInstance().getReference(Constants.Reference.REFERENCE_FRIENDS)
                .child(uid)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        userArrayList.clear()
                        for (snapshot in dataSnapshot.children) {
                            val user = snapshot.getValue(Users::class.java)
                            if (user != null) {
                                if (!user.id.equals(uid)) {
                                    userArrayList.add(user)
                                }
                            }
                        }
                        readUserFriend(userArrayList)
                    }

                    override fun onCancelled(databaseError: DatabaseError) {}
                })
        }
    }

    private fun readUserFriend(userArrayList: ArrayList<Users>) {
        FirebaseDatabase.getInstance().getReference(Constants.Reference.REFERENCE_USERS)
            .addValueEventListener(object : ValueEventListener {
                @RequiresApi(api = Build.VERSION_CODES.N)
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    users.clear()
                    for (snapshot in dataSnapshot.children) {
                        val user = snapshot.getValue(Users::class.java)
                        for (i in userArrayList.indices) {
                            if (user != null && user.id.equals(userArrayList[i].id)) {
                                users.add(user)
                            }
                        }
                    }
                    sortListUserWithAlphabet(users)
                }
                override fun onCancelled(databaseError: DatabaseError) {}
            })
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private fun sortListUserWithAlphabet(users: ArrayList<Users>) {
        val sortedUsers = users.sortedBy {it.username?.first()?.toUpperCase().toString()}
        this.users = ArrayList(sortedUsers)
        filterListUser()
    }

    private fun filterListUser() {
        sortUsers = ArrayList()
        var lastHeader = ""
        for (user in users) {
            val header: String =
                java.lang.String.valueOf(user.username!!.first()).toUpperCase(Locale.ROOT)
            if (header != lastHeader) {
                lastHeader = header
                sortUsers.add(FriendSort(header, null, true))
            }
            sortUsers.add(FriendSort(header, user, false))
        }
        resultFriendSort.postValue(sortUsers)
    }

    fun searchUser(word: String) {
        val userArrayList = ArrayList<Users?>()
        val userCurrent = Firebase.auth.currentUser
        userCurrent?.let {
            val uid: String = it.uid
            FirebaseDatabase.getInstance().getReference(Constants.Reference.REFERENCE_FRIENDS)
                .child(uid)
                .addValueEventListener(object : ValueEventListener {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    override fun onDataChange(snapshot: DataSnapshot) {
                        userArrayList.clear()
                        for (dataSnapshot in snapshot.children) {
                            val user = dataSnapshot.getValue(Users::class.java)!!
                            if (!user.id.equals(uid)) {
                                userArrayList.add(user)
                            }
                        }
                        readUserSearch(userArrayList, word)
                    }
                    override fun onCancelled(error: DatabaseError) {}
                })
        }
    }

    private fun readUserSearch(userArrayList: ArrayList<Users?>, word: String) {
        FirebaseDatabase.getInstance().getReference(Constants.Reference.REFERENCE_USERS)
            .orderByChild(Constants.Firebase.FIRE_BASE_SEARCH)
            .startAt(word).endAt(word + "\uf8ff")
            .addValueEventListener(object : ValueEventListener {
                @RequiresApi(api = Build.VERSION_CODES.N)
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    users.clear()
                    for (snapshot in dataSnapshot.children) {
                        val user = snapshot.getValue(Users::class.java)
                        for (i in userArrayList.indices) {
                            if (user != null && user.id.equals(userArrayList[i]!!.id)) {
                                users.add(user)
                            }
                        }
                    }
                    sortListUser(users)
                }

                override fun onCancelled(databaseError: DatabaseError) {}
            })
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private fun sortListUser(userArrayList: ArrayList<Users>) {
        val sortedUserSearch = userArrayList.sortedBy {it.username?.first()?.toUpperCase().toString()}
        this.userArrayList = ArrayList(sortedUserSearch)
        filterListUserSearch(userArrayList)
    }

    private fun filterListUserSearch(userArrayList: ArrayList<Users>) {
        val usersSortSearch: ArrayList<FriendSort> = ArrayList()
        var lastHeader = ""
        for (user in userArrayList) {
            val header: String =
                java.lang.String.valueOf(user.username!!.first()).toUpperCase(Locale.ROOT)
            if (header != lastHeader) {
                lastHeader = header
                usersSortSearch.add(FriendSort(header, null, true))
            }
            usersSortSearch.add(FriendSort(header, user, false))
        }
        resultFriendSort.postValue(usersSortSearch)
    }

}