package mvvm.chat.ui.tabfriend

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.mvvm.R
import com.android.mvvm.databinding.FragmentTabFriendBinding
import mvvm.chat.base.BaseFragment
import mvvm.chat.ui.homefriend.HomeFriendFragment
import mvvm.chat.utils.Constants
import java.util.ArrayList

class TabFriendFragment : BaseFragment() {

    private lateinit var viewModel: TabFriendViewModel
    private lateinit var binding: FragmentTabFriendBinding

    private var adapter: TabFriendAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTabFriendBinding.inflate(inflater, container, false).apply {
            viewModel =
                ViewModelProviders.of(this@TabFriendFragment).get(TabFriendViewModel::class.java)
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupObservers()
        setInitAdapter()
        setListeners()
        view.setOnTouchListener { _, _ ->
            hideSoftKeyboard(requireActivity())
            false
        }
    }

    private fun setListeners() {
        val homeFriendFragment = parentFragment as HomeFriendFragment?
        homeFriendFragment?.setOnTextChanged(object : HomeFriendFragment.OnTextChanged {
            override fun onTextChanged(word: String?) {
                if (word != null) {
                    viewModel.searchUser(word)
                }
            }
        })
    }

    private fun setInitAdapter() {
        binding.recyclerViewTabFriend.setHasFixedSize(true)
        adapter = TabFriendAdapter(requireContext(), ArrayList())
        binding.recyclerViewTabFriend.adapter = adapter
    }

    private fun setupObservers() {
        viewModel.resultFriendSort.observe(viewLifecycleOwner) {
            it?.let {
                if (adapter != null) {
                    adapter!!.setData(it)
                    adapter!!.setOnClick(object : TabFriendAdapter.IOnClick {
                        override fun onItemCLick(i: Int) {
                            val bundle = Bundle()
                            bundle.putSerializable(Constants.Bundle.BUNDLE_KEY_USERS, it[i].users)
                            NavHostFragment.findNavController(this@TabFriendFragment)
                                .navigate(R.id.action_tabFriend_to_userProfileFragment, bundle)
                        }
                    })
                }
            }
        }
    }
}