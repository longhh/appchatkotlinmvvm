package mvvm.chat.ui.tabfriend

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.mvvm.R
import com.android.mvvm.databinding.ItemTabFriendBinding
import com.android.mvvm.databinding.ItemTabFriendHeaderBinding
import com.bumptech.glide.Glide
import mvvm.chat.data.FriendSort
import mvvm.chat.utils.Constants

class TabFriendAdapter (private val context: Context, private var friendSortList: List<FriendSort>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val sectionView = 1
    private val contentView = 2

    interface IOnClick {
        fun onItemCLick(i: Int)
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setData(friendSortList: List<FriendSort>) {
        this.friendSortList = friendSortList
        notifyDataSetChanged()
    }

    private var iOnClick: IOnClick? = null

    fun setOnClick(iOnClick: IOnClick) {
        this.iOnClick = iOnClick
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): RecyclerView.ViewHolder {
        return when (i) {
            sectionView -> {
                MyViewHolderHead(ItemTabFriendHeaderBinding
                    .inflate(LayoutInflater.from(viewGroup.context), viewGroup, false))
            }
            else -> {
                MyViewHolder(ItemTabFriendBinding
                    .inflate(LayoutInflater.from(viewGroup.context), viewGroup, false))
            }
        }
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, i: Int) {
        val user: FriendSort = friendSortList[i]
        when (viewHolder.itemViewType) {
            sectionView -> {
                val myViewHolderHead: MyViewHolderHead = viewHolder as MyViewHolderHead
                myViewHolderHead.bindView(user)
            }
            contentView -> {
                val myViewHolder: MyViewHolder = viewHolder as MyViewHolder
                myViewHolder.bindView(user, iOnClick, context)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (friendSortList[position].isSection) {
            sectionView
        } else {
            contentView
        }
    }

    override fun getItemCount(): Int {
        return friendSortList.size
    }

    class MyViewHolder(val binding: ItemTabFriendBinding) : RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("CheckResult")
        fun bindView(user: FriendSort, iOnClick: IOnClick?, context: Context) {
            binding.txtUsername.text = user.users!!.username
            binding.itemTabFriend.setOnClickListener { iOnClick?.onItemCLick(adapterPosition) }
            if (user.users!!.imageURL.equals(Constants.Firebase.FIRE_BASE_DEFAULT)) {
                Glide.with(context).load(R.drawable.ic_avt).circleCrop().into(binding.imgCircleFriend)
            } else {
                Glide.with(context).load(user.users!!.imageURL).circleCrop().into(binding.imgCircleFriend)
            }
        }
    }

    class MyViewHolderHead(val binding: ItemTabFriendHeaderBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bindView(user: FriendSort) {
            binding.txtHeader.text = user.header
        }
    }
}