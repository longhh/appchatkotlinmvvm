package mvvm.chat.ui.tabrequest

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.android.mvvm.databinding.FragmentTabRequestBinding
import mvvm.chat.base.BaseFragment
import mvvm.chat.data.Users

class TabRequestFragment : BaseFragment() {
    private lateinit var viewModel: TabRequestViewModel
    private lateinit var binding: FragmentTabRequestBinding

    private var adapterReceiver: ReceiverAdapter? = null
    private var adapterSender: SenderAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTabRequestBinding.inflate(inflater, container, false).apply {
            viewModel =
                ViewModelProviders.of(this@TabRequestFragment).get(TabRequestViewModel::class.java)
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupObservers()
        initAdapter()
        view.setOnTouchListener { _, _ ->
            hideSoftKeyboard(requireActivity())
            false
        }
    }

    private fun initAdapter() {
        adapterReceiver = ReceiverAdapter(requireContext(), ArrayList())
        binding.rvRequestFriend.adapter = adapterReceiver
        adapterSender = SenderAdapter(requireContext(), ArrayList())
        binding.rvSendRequestFriend.adapter = adapterSender

        adapterReceiver!!.setOnClickItem(object : ReceiverAdapter.IOnClickItem {
            override fun itemClick(users: Users?) {
                viewModel.agreeRequest(users!!)
            }

            override fun deleteItem(users: Users?) {
                viewModel.disagreeRequest(users!!)
            }
        })
        adapterSender!!.setOnClickItemSender(object : SenderAdapter.IOnClickItemSender {
            override fun itemClick(users: Users?) {
                viewModel.cancelRequest(users!!)
            }
        })
    }


    private fun setupObservers() {
        viewModel.resultSender.observe(viewLifecycleOwner, {
            it?.let {
                adapterSender!!.setData(it)
            }
        })
        viewModel.resultReceiver.observe(viewLifecycleOwner, {
            it?.let {
                adapterReceiver!!.setData(it)
            }
        })
    }
}