package mvvm.chat.ui.tabrequest

import androidx.lifecycle.MutableLiveData
import mvvm.chat.base.BaseViewModel
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.*
import com.google.firebase.ktx.Firebase
import mvvm.chat.data.Users
import mvvm.chat.utils.Constants

class TabRequestViewModel : BaseViewModel() {
    val resultSender = MutableLiveData<List<Users>>()
    val resultReceiver = MutableLiveData<List<Users>>()
    var sender: Users? = null

    init {
        readSender()
        readReceiver()
    }

    private fun readSender() {
        getDataSender()
        val userArrayList: ArrayList<Users> = ArrayList()
        val userCurrent = Firebase.auth.currentUser
        userCurrent?.let {
            val uid: String = it.uid
            FirebaseDatabase.getInstance()
                .getReference(Constants.Reference.REFERENCE_FRIEND_REQUEST)
                .child(uid)
                .child(Constants.Reference.REFERENCE_REQUEST_SEND)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        userArrayList.clear()
                        for (snapshot in dataSnapshot.children) {
                            val user: Users? = snapshot.getValue(Users::class.java)
                            if (user != null) {
                                if (!user.id.equals(uid)) {
                                    userArrayList.add(user)
                                }
                            }
                        }
                        resultSender.postValue(userArrayList)
                    }

                    override fun onCancelled(databaseError: DatabaseError) {}
                })
        }
    }

    private fun getDataSender() {
        val userCurrent = Firebase.auth.currentUser
        userCurrent?.let {
            val uid: String = it.uid
            FirebaseDatabase.getInstance()
                .getReference(Constants.Reference.REFERENCE_USERS)
                .child(uid)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        sender = dataSnapshot.getValue(Users::class.java)
                    }

                    override fun onCancelled(databaseError: DatabaseError) {
                    }
                })
        }
    }

    private fun readReceiver() {
        val userArrayList: ArrayList<Users> = ArrayList()
        val userCurrent = Firebase.auth.currentUser
        userCurrent?.let {
            val uid: String = it.uid
            FirebaseDatabase.getInstance()
                .getReference(Constants.Reference.REFERENCE_FRIEND_REQUEST)
                .child(uid)
                .child(Constants.Reference.REFERENCE_REQUEST_RECEIVE)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        userArrayList.clear()
                        for (snapshot in dataSnapshot.children) {
                            val user: Users? = snapshot.getValue(Users::class.java)
                            if (user != null) {
                                if (!user.id.equals(uid)) {
                                    userArrayList.add(user)
                                }
                            }
                        }
                        resultReceiver.postValue(userArrayList)
                    }

                    override fun onCancelled(databaseError: DatabaseError) {}
                })
        }
    }

    private fun removeValueSender(users: Users?, id: String, table: String) {
        val query: Query = FirebaseDatabase.getInstance().reference
            .child(Constants.Reference.REFERENCE_FRIEND_REQUEST)
            .child(id)
            .child(table)
            .orderByChild(Constants.Firebase.FIRE_BASE_ID).equalTo(users!!.id)
        query.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (appleSnapshot in dataSnapshot.children) {
                    appleSnapshot.ref.removeValue()
                    readSender()
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {}
        })
    }

    fun cancelRequest(users: Users) {
        val userCurrent = Firebase.auth.currentUser
        userCurrent?.let {
            val uid: String = it.uid
            removeValueSender(
                users,
                uid,
                Constants.Reference.REFERENCE_REQUEST_SEND
            )
            removeValueSender(sender, users.id!!, Constants.Reference.REFERENCE_REQUEST_RECEIVE)
        }
    }

    fun agreeRequest(users: Users) {
        val userCurrent = Firebase.auth.currentUser
        userCurrent?.let {
            val uid: String = it.uid
            removeValueSender(
                users,
                uid,
                Constants.Reference.REFERENCE_REQUEST_RECEIVE
            )
            removeValueSender(sender, users.id!!, Constants.Reference.REFERENCE_REQUEST_SEND)
            FirebaseDatabase.getInstance()
                .getReference(Constants.Reference.REFERENCE_FRIENDS)
                .child(uid)
                .push()
                .setValue(users)
            FirebaseDatabase.getInstance()
                .getReference(Constants.Reference.REFERENCE_FRIENDS)
                .child(users.id!!)
                .push()
                .setValue(sender)
        }
    }

    fun disagreeRequest(users: Users) {
        val userCurrent = Firebase.auth.currentUser
        userCurrent?.let {
            val uid: String = it.uid
            removeValueSender(
                users,
                uid,
                Constants.Reference.REFERENCE_REQUEST_RECEIVE
            )
            removeValueSender(sender, users.id!!, Constants.Reference.REFERENCE_REQUEST_SEND)
        }
    }
}