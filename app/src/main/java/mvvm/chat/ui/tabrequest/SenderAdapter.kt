package mvvm.chat.ui.tabrequest

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.mvvm.R
import com.android.mvvm.databinding.ItemTabRequestCancelBinding
import com.bumptech.glide.Glide
import mvvm.chat.data.Users
import mvvm.chat.utils.Constants

class SenderAdapter(private val context: Context, private var usersList: List<Users>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>(){

    interface IOnClickItemSender {
        fun itemClick(users: Users?)
    }

    private var iOnClickItem: IOnClickItemSender? = null

    fun setOnClickItemSender(iOnClickItem: IOnClickItemSender?) {
        this.iOnClickItem = iOnClickItem
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setData(usersList: List<Users>) {
        this.usersList = usersList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): SenderViewHolder {
        return SenderViewHolder(
            ItemTabRequestCancelBinding.inflate(
                LayoutInflater.from(viewGroup.context), viewGroup, false
            )
        )
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, i: Int) {
        val users = usersList[i]
        val senderViewHolder = viewHolder as SenderViewHolder
        senderViewHolder.bindView(users, iOnClickItem, context)
    }

    override fun getItemCount(): Int {
        return usersList.size
    }

    class SenderViewHolder(val binding: ItemTabRequestCancelBinding) : RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("CheckResult")
        fun bindView(users: Users, iOnClickItem: IOnClickItemSender?, context: Context) {
            binding.txtUsername.text = users.username
            if (users.imageURL.equals(Constants.Firebase.FIRE_BASE_DEFAULT)) {
                Glide.with(context).load(R.drawable.ic_avt)
            } else {
                Glide.with(context).load(users.imageURL).into(binding.imgCircleFriend)
            }
            binding.btnCancelRequest.setOnClickListener { iOnClickItem?.itemClick(users) }
        }
    }
}