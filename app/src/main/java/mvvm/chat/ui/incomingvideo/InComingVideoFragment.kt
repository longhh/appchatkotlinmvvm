package mvvm.chat.ui.incomingvideo

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.android.mvvm.databinding.FragmentIncomingVideoBinding
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.ktx.Firebase
import mvvm.chat.base.BaseFragment
import mvvm.chat.data.Room
import mvvm.chat.utils.Constants
import org.jitsi.meet.sdk.BroadcastEvent
import org.jitsi.meet.sdk.JitsiMeet
import org.jitsi.meet.sdk.JitsiMeetActivity
import org.jitsi.meet.sdk.JitsiMeetConferenceOptions
import timber.log.Timber
import java.net.MalformedURLException
import java.net.URL


class InComingVideoFragment : BaseFragment() {

    private val broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            onBroadcastReceived(intent)
        }
    }

    private lateinit var binding: FragmentIncomingVideoBinding
    private lateinit var viewModel: InComingVideoViewModel
    private var roomId: String? = null
    private var receiver: String? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentIncomingVideoBinding.inflate(inflater, container, false).apply {
            viewModel =
                ViewModelProviders.of(this@InComingVideoFragment)
                    .get(InComingVideoViewModel::class.java)
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        view.setOnTouchListener { _, _ ->
            hideSoftKeyboard(requireActivity())
            false
        }
        val serverURL: URL = try {
            URL("https://meet.jit.si")
        } catch (e: MalformedURLException) {
            e.printStackTrace()
            throw RuntimeException("Invalid server URL!")
        }
        val defaultOptions = JitsiMeetConferenceOptions.Builder()
            .setServerURL(serverURL)
            .setFeatureFlag("welcomepage.enabled", false)
            .build()
        JitsiMeet.setDefaultConferenceOptions(defaultOptions)

        registerForBroadcastMessages()
    }

    private fun initView() {
        val user = Firebase.auth.currentUser
        if (user != null) {
            FirebaseDatabase.getInstance().reference.child(Constants.Reference.REFERENCE_ROOMS)
                .child(user.uid)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        for (dataSnapshot in snapshot.children) {
                            val room: Room? = dataSnapshot.getValue(Room::class.java)
                            if (room != null) {
                                if (room.calling.equals("receiver")) {
                                    receiver = room.receiver
                                    roomId = room.id
                                }

                            }
                        }
                    }

                    override fun onCancelled(error: DatabaseError) {
                    }
                })
            binding.btnStartCalling.setOnClickListener {
                val map = HashMap<String, Any>()
                map[Constants.Firebase.CALLING] = roomId.toString()
                FirebaseDatabase.getInstance()
                    .getReference(Constants.Reference.REFERENCE_ROOMS)
                    .child(user.uid)
                    .child(roomId.toString())
                    .updateChildren(map)

                FirebaseDatabase.getInstance()
                    .getReference(Constants.Reference.REFERENCE_ROOMS)
                    .child(receiver.toString())
                    .child(roomId.toString())
                    .updateChildren(map)

                val options = JitsiMeetConferenceOptions.Builder()
                    .setRoom(roomId.toString())
                    .build()
                JitsiMeetActivity.launch(requireContext(), options)
            }

            val map = java.util.HashMap<String, Any>()
            map[Constants.Firebase.CALLING] = "false"

            binding.btnCancelCalling.setOnClickListener {
                activity?.fragmentManager?.popBackStack()
                roomId?.let { it1 ->
                    FirebaseDatabase.getInstance()
                        .getReference(Constants.Reference.REFERENCE_ROOMS)
                        .child(receiver!!)
                        .child(it1)
                        .updateChildren(map)

                    FirebaseDatabase.getInstance()
                        .getReference(Constants.Reference.REFERENCE_ROOMS)
                        .child(user.uid)
                        .child(it1)
                        .updateChildren(map)
                }
            }
        }
    }

    private fun registerForBroadcastMessages() {
        val intentFilter = IntentFilter()
        for (type in BroadcastEvent.Type.values()) {
            intentFilter.addAction(type.action)
        }
        LocalBroadcastManager.getInstance(requireContext())
            .registerReceiver(broadcastReceiver, intentFilter)
    }

    private fun onBroadcastReceived(intent: Intent?) {
        if (intent != null) {
            val event = BroadcastEvent(intent)
            when (event.type) {
                BroadcastEvent.Type.CONFERENCE_JOINED -> Timber.i(
                    "Conference Joined with url%s",
                    event.data["url"]
                )
                BroadcastEvent.Type.PARTICIPANT_JOINED -> Timber.i(
                    "Participant joined%s",
                    event.data["name"]
                )
            }
        }
    }
}