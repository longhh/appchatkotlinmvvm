package mvvm.chat.ui.homeprofile

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.res.Configuration
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.core.app.ActivityCompat.recreate
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import com.android.mvvm.R
import com.android.mvvm.databinding.DialogConfirmPasswordBinding
import com.android.mvvm.databinding.DialogSignOutBinding
import com.android.mvvm.databinding.FragmentHomeProfileBinding
import com.bumptech.glide.Glide
import mvvm.chat.base.BaseFragment
import mvvm.chat.utils.Constants
import java.util.*


class HomeProfileFragment : BaseFragment() {

    private lateinit var viewModel: HomeProfileViewModel
    private lateinit var binding: FragmentHomeProfileBinding

    private var listLanguage: ArrayList<String> = ArrayList()
    private var listItem = arrayOf("Tiếng Việt", "English")
    private var isPasswordUpdated = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeProfileBinding.inflate(inflater, container, false).apply {
            viewModel = ViewModelProviders.of(this@HomeProfileFragment)
                .get(HomeProfileViewModel::class.java)
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadLocate()
        setupObservers()
        setListeners()
        view.setOnTouchListener { _, _ ->
            hideSoftKeyboard(requireActivity())
            false
        }
    }

    private fun setListeners() {
        binding.imgChangeProfile.setOnClickListener {
            NavHostFragment.findNavController(this)
                .navigate(R.id.action_homeFragment_to_changeProfileFragment, null)
        }
        binding.txtSignOut.setOnClickListener {
            dialogSignOut()
        }
        binding.changeLanguage.setOnClickListener {
            showChangeLanguageDialog()
        }
        binding.languageCurrent.setOnClickListener {
            showChangeLanguageDialog()
        }
        binding.btnChangePassword.setOnClickListener {
            showDialogConfirmPassword()
        }
    }

    private fun showDialogConfirmPassword() {
        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

        val bindingDialog: DialogConfirmPasswordBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context), R.layout.dialog_confirm_password, null, false
        )
        dialog.setContentView(bindingDialog.root)
        val window = dialog.window
        window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val windowAttribute: WindowManager.LayoutParams = window?.attributes!!
        windowAttribute.gravity = Gravity.CENTER
        window.attributes = windowAttribute

        bindingDialog.btnCancel.setOnClickListener {
            dialog.dismiss()
        }

        bindingDialog.btnConfirm.setOnClickListener {
            viewModel.changePassword(
                bindingDialog.edtNewPassword.text.toString().trim(),
                bindingDialog.edtEmail.text.toString().trim(),
                bindingDialog.edtOldPassword.text.toString().trim()
            )
        }
        if (isPasswordUpdated) {
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun showChangeLanguageDialog() {
        listLanguage.add("vi")
        listLanguage.add("en")
        val builder = AlertDialog.Builder(context)
            .setTitle(R.string.choose_language)
            .setSingleChoiceItems(
                listItem,
                getIndexLanguage(Locale.getDefault().language)
            ) { dialog, which ->
                if (which == 0) {
                    setLocate("vi")
                    recreate(requireActivity())
                } else if (which == 1) {
                    setLocate("en")
                    recreate(requireActivity())
                }
                dialog.dismiss()
            }
        val alertDialog = builder.create()
        alertDialog.show()
    }

    private fun getIndexLanguage(locale: String): Int {
        for (i in listLanguage.indices) {
            if (listLanguage[i] == locale) {
                return listLanguage.indexOf(locale)
            }
        }
        return -1
    }

    private fun setLocate(lang: String) {
        val locale = Locale(lang)
        Locale.setDefault(locale)
        val config = Configuration()
        config.locale = locale
        context?.resources?.updateConfiguration(config, requireContext().resources.displayMetrics)

        val editor = activity?.getSharedPreferences("Settings", Context.MODE_PRIVATE)?.edit()
        editor?.putString("MyLang", lang)
        editor?.apply()
    }

    private fun loadLocate() {
        val sharedPreferences = activity?.getSharedPreferences("Settings", Activity.MODE_PRIVATE)
        val language = sharedPreferences?.getString("MyLang", "en")
        language?.let { setLocate(it) }
    }

    private fun dialogSignOut() {
        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

        val bindingDialog: DialogSignOutBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context), R.layout.dialog_sign_out, null, false
        )
        dialog.setContentView(bindingDialog.root)
        val window = dialog.window
        window?.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val windowAttribute: WindowManager.LayoutParams = window?.attributes!!
        windowAttribute.gravity = Gravity.CENTER
        window.attributes = windowAttribute

        bindingDialog.btnConfirm.setOnClickListener {
            signOut()
            dialog.dismiss()
        }
        bindingDialog.btnCancel.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun signOut() {
        viewModel.signOut()
        NavHostFragment.findNavController(this)
            .navigate(R.id.action_homeFragment_to_loginFragment, null)
    }

    private fun setupObservers() {
        viewModel.resultEmail.observe(viewLifecycleOwner) {
            it?.let {
                binding.txtEmailProfile.text = it
            }
        }
        viewModel.resultUser.observe(viewLifecycleOwner) {
            it?.let {
                binding.txtNameProfile.text = it.username
                if (context != null) {
                    if (it.imageURL.equals(Constants.Firebase.FIRE_BASE_DEFAULT)) {
                        Glide.with(requireContext()).load(R.drawable.ic_avt)
                            .into(binding.imgCircleProfile)
                        Glide.with(requireContext()).load(R.drawable.ic_avt)
                            .into(binding.imgProfile)
                    } else {
                        Glide.with(requireContext()).load(it.imageURL)
                            .into(binding.imgCircleProfile)
                        Glide.with(requireContext()).load(it.imageURL).into(binding.imgProfile)
                    }
                }
            }
        }
        viewModel.resultChangePassword.observe(viewLifecycleOwner) {
            if (it) {
                isPasswordUpdated = it
                Toast.makeText(requireContext(), getString(R.string.password_updated), Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(requireContext(), getString(R.string.invalid_email_password), Toast.LENGTH_SHORT).show()
            }
        }
        viewModel.progressBar.observe(viewLifecycleOwner) {
            if (it) {
                binding.progressBar.visibility = View.VISIBLE
            } else {
                binding.progressBar.visibility = View.GONE
            }
        }
    }
}