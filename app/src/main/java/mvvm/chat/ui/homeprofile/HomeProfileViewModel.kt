package mvvm.chat.ui.homeprofile

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import mvvm.chat.base.BaseViewModel
import mvvm.chat.data.Users
import mvvm.chat.utils.Constants

class HomeProfileViewModel : BaseViewModel() {
    val resultEmail = MutableLiveData<String>()
    val resultUser = MutableLiveData<Users>()
    val resultChangePassword = MutableLiveData<Boolean>()
    val progressBar = MutableLiveData<Boolean>()


    init {
        showProfile()
    }

    private fun showProfile() {
        val handlerException = CoroutineExceptionHandler { _, throwable ->
            println("CoroutineExceptionHandler got $throwable")
        }
        CoroutineScope(Dispatchers.IO + handlerException).launch {
            progressBar.postValue(true)
            try {
                val user = Firebase.auth.currentUser
                user?.let {
                    val email: String? = user.email
                    resultEmail.postValue(email)
                    val uid: String = user.uid
                    FirebaseDatabase.getInstance().getReference(Constants.Firebase.FIRE_BASE_USERS)
                        .child(uid)
                        .addValueEventListener(object : ValueEventListener {
                            override fun onDataChange(dataSnapshot: DataSnapshot) {
                                val users: Users? = dataSnapshot.getValue(Users::class.java)
                                if (users != null) {
                                    resultUser.postValue(users)
                                    progressBar.postValue(false)
                                }
                            }

                            override fun onCancelled(databaseError: DatabaseError) {}
                        })
                }
            } catch (e: Exception) {
                Log.getStackTraceString(e)
            }
        }
    }

    fun signOut() {
        FirebaseAuth.getInstance().signOut()
    }

    fun changePassword(newPassword: String, email: String, password: String) {
        val user = Firebase.auth.currentUser
        if (newPassword.isNotEmpty()) {
            user!!.updatePassword(newPassword)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        resultChangePassword.postValue(true)
                    } else {
                        reAuthenticateUser(newPassword, email, password)
                    }
                }
        }
    }

    private fun reAuthenticateUser(newPassword: String, email: String, password: String) {
        val user = Firebase.auth.currentUser!!
        if (password.isNotEmpty() && email.isNotEmpty()) {
            val credential = EmailAuthProvider.getCredential(email, password)
            user.reauthenticate(credential)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        changePassword(newPassword, email, password)
                    } else {
                        resultChangePassword.postValue(false)
                    }
                }
        }
    }
}