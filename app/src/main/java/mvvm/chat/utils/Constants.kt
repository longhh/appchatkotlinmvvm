package mvvm.chat.utils

import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class Constants {
    companion object {
        const val PREF_FILE_NAME = "Preferences"
        const val BASE_URL = "https://api.github.com/"
        const val REQUEST_TIMEOUT_DURATION = 10
        const val DEBUG = true
        const val ACTION_HAVE_CALLING = "ACTION_HAVE_CALLING"

        var stickers: List<String> = listOf(
            "https://fcbk.su/_data/stickers/playing_with_drogo/playing_with_drogo_10.png",
            "https://fcbk.su/_data/stickers/playing_with_drogo/playing_with_drogo_02.png",
            "https://fcbk.su/_data/stickers/playing_with_drogo/playing_with_drogo_07.png",
            "https://fcbk.su/_data/stickers/playing_with_drogo/playing_with_drogo_08.png",
            "https://fcbk.su/_data/stickers/playing_with_drogo/playing_with_drogo_19.png",
            "https://fcbk.su/_data/stickers/playing_with_drogo/playing_with_drogo_10.png",
            "https://fcbk.su/_data/stickers/playing_with_drogo/playing_with_drogo_11.png",
            "https://fcbk.su/_data/stickers/playing_with_drogo/playing_with_drogo_12.png",
            "https://fcbk.su/_data/stickers/playing_with_drogo/playing_with_drogo_13.png"
        )

        const val REMOTE_MSG_AUTHORIZATION = "Authorization"
        const val REMOTE_MSG_CONTENT_TYPE = "Content-Type"
        const val REMOTE_MSG_TYPE = "type"
        const val REMOTE_MSG_INVITATION = "invitation"
        const val REMOTE_MSG_MEETING_TYPE = "meetingType"
        const val REMOTE_MSG_INVITER_TOKEN = "inviterToken"
        const val REMOTE_MSG_DATA = "data"
        const val REMOTE_MSG_REGISTRATION_IDS = "registration_ids"

        fun getRemoteMessageHeaders(): HashMap<String, String> {
            val headers: HashMap<String, String> = HashMap()
            headers[Constants.REMOTE_MSG_AUTHORIZATION] = "key=AAAA2eMiKuc:APA91bFkhQzsWTlDNxDqErW5A0NVIXt6CIvH_O7Vx4aFdgOTXWdJbADLWN605n-pc6IugyNsdxZ9q44FRYoyMW-kh_rAqC_sw4nzSF2wpHVaRG-1652z65HjHVOzJoawtAWZ4PbKIyPq"
            headers[Constants.REMOTE_MSG_CONTENT_TYPE] = "application/json"
            return headers
        }
    }

    object Bundle {
        const val BUNDLE_PHONE_NUMBER = "PHONE_NUMBER"
        const val BUNDLE_VERIFY = "VERIFY"
        const val BUNDLE_KEY_ROOM = "ROOM"
        const val BUNDLE_KEY_USERS = "USERS"
        const val BUNDLE_CONTENT = "content"
        const val BUNDLE_KEY_NAME = "nameChanged"
        const val BUNDLE_KEY_IMAGE = "imageChanged"
        const val EMAIL = "email"
    }

    object Firebase {
        const val FIRE_BASE_DEFAULT = "default"
        const val FIRE_BASE_USERS = "Users"
        const val FIRE_BASE_ID = "id"
        const val FIRE_BASE_USERNAME = "username"
        const val FIRE_BASE_IMAGE_URL = "imageURL"
        const val FIRE_BASE_SEARCH = "search"
        const val FIRE_BASE_PHONE_NUMBER = "phoneNumber"
        const val FIRE_BASE_DATE_OF_BIRTH = "dateOfBirth"
        const val FIRE_BASE_ADDRESS = "address"
        const val FIRE_BASE_EMAIL = "email"
        const val FIRE_BASE_TYPING = "typing"
        const val FIRE_BASE_SEARCH_BY_NAME = "searchByName"
        const val CALLING = "calling"
        const val FIRE_BASE_CONTENT = "content"
        const val UNREAD_MESSAGE = "unreadMessage"
    }


    object Reference {
        const val REFERENCE_USERS = "Users"
        const val REFERENCE_ROOMS = "Rooms"
        const val REFERENCE_MEMBERS = "Members"
        const val REFERENCE_MESSAGES = "Messages"
        const val REFERENCE_FRIENDS = "Friends"
        const val REFERENCE_TOKENS = "Tokens"
        const val REFERENCE_FRIEND_REQUEST = "FriendRequest"
        const val REFERENCE_REQUEST_SEND = "RequestSend"
        const val REFERENCE_REQUEST_RECEIVE = "RequestReceive"
    }


    object TimeFormat {
        const val FULL = "dd-MM-yyyy HH:mm:ss"
        const val DAY = "dd"
        const val MONTH = "MM"
        const val DATE = "dd-MM-yyyy"
        const val HOURS = "HH:mm"
    }



    enum class StateUser {
        FRIEND, REQUEST_FRIEND, OTHERS
    }

    object Notification {
        const val USER = "user"
        const val ICON = "icon"
        const val TITLE = "title"
        const val BODY = "body"
        const val SENT = "sent"
    }
}