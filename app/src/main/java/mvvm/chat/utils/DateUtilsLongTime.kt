package mvvm.chat.utils

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

class DateUtilsLongTime(longTime: Long) {
    private var longTime: Long = 0

    @SuppressLint("SimpleDateFormat")
    var full = SimpleDateFormat(Constants.TimeFormat.FULL)

    @SuppressLint("SimpleDateFormat")
    var monthFormat = SimpleDateFormat(Constants.TimeFormat.MONTH)

    @SuppressLint("SimpleDateFormat")
    var dayFormat = SimpleDateFormat(Constants.TimeFormat.DAY)

    @SuppressLint("SimpleDateFormat")
    var hourFormat = SimpleDateFormat(Constants.TimeFormat.HOURS)

    @SuppressLint("SimpleDateFormat")
    var dateFormat = SimpleDateFormat(Constants.TimeFormat.DATE)

    init {
        this.longTime = longTime
    }


    fun getMonthLong(): String? {
        return monthFormat.format(longTime)
    }

    fun getDateLong(): String? {
        return dateFormat.format(longTime)
    }

    fun getDayLong(): String? {
        return dayFormat.format(longTime)
    }

    fun getHourLong(): String? {
        return hourFormat.format(longTime)
    }
}