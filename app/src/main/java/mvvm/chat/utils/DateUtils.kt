package mvvm.chat.utils

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

class DateUtils(date: Date) {
    private var date: Date? = null

    @SuppressLint("SimpleDateFormat")
    var full = SimpleDateFormat(Constants.TimeFormat.FULL)

    @SuppressLint("SimpleDateFormat")
    var monthFormat = SimpleDateFormat(Constants.TimeFormat.MONTH)

    @SuppressLint("SimpleDateFormat")
    var dayFormat = SimpleDateFormat(Constants.TimeFormat.DAY)

    @SuppressLint("SimpleDateFormat")
    var hourFormat = SimpleDateFormat(Constants.TimeFormat.HOURS)

    @SuppressLint("SimpleDateFormat")
    var dateFormat = SimpleDateFormat(Constants.TimeFormat.DATE)

    init {
        this.date = date
    }

    fun getMonth(): String? {
        return monthFormat.format(date)
    }

    fun getDate(): String? {
        return dateFormat.format(date)
    }

    fun getDay(): String? {
        return dayFormat.format(date)
    }

    fun getHour(): String? {
        return hourFormat.format(date)
    }

}