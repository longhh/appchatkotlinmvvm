package mvvm.chat.utils.listener

interface OnLongClickListener {
    fun transferMessageText(position: Int, content: String)
    fun transferMessageImage(position: Int, content: String)
}