package mvvm.chat.utils.listener

interface OnItemClickListener {
    fun sendRequestFriend(position: Int)
    fun cancelRequestFriend(position: Int)
    fun onItemClick(position: Int)
}