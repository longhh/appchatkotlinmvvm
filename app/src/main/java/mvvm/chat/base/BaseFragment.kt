package mvvm.chat.base

import android.app.Activity
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import mvvm.MainActivity


abstract class BaseFragment: Fragment() {

    private val mActivity: MainActivity? = null
    open fun hideSoftKeyboard(activity: Activity) {
        val inputMethodManager: InputMethodManager = activity.getSystemService(
            Activity.INPUT_METHOD_SERVICE
        ) as InputMethodManager
        if (inputMethodManager.isAcceptingText) {
            inputMethodManager.hideSoftInputFromWindow(
                activity.currentFocus!!.windowToken,
                0
            )
        }
    }

    open fun hideKeyboard() {
        mActivity?.hideKeyboard()
    }
}