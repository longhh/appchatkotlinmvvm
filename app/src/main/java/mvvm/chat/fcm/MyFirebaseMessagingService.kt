package mvvm.chat.fcm

import android.annotation.SuppressLint
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.content.res.ResourcesCompat
import com.android.mvvm.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import mvvm.MainActivity
import mvvm.MvvmChatApp
import mvvm.chat.utils.Constants

class MyFirebaseMessagingService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        val sent = remoteMessage.data[Constants.Notification.SENT]
        val firebaseUser = FirebaseAuth.getInstance().currentUser
        if (firebaseUser != null && sent != null && sent == firebaseUser.uid) {
            sendNotification(remoteMessage)
        }
    }

    @SuppressLint("UnspecifiedImmutableFlag")
    private fun sendNotification(remoteMessage: RemoteMessage) {
        val user: String? = remoteMessage.data[Constants.Notification.USER]
        val icon: String? = remoteMessage.data[Constants.Notification.ICON]
        val title: String? = remoteMessage.data[Constants.Notification.TITLE]
        val body: String? = remoteMessage.data[Constants.Notification.BODY]
        assert(user != null)
        val j = user!!.replace("[\\D]".toRegex(), "").toInt()
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = if (Build.VERSION.SDK_INT < Build.VERSION_CODES.S) {
            PendingIntent.getActivity(this, j, intent, PendingIntent.FLAG_ONE_SHOT)
        } else {
            PendingIntent.getActivity(this, j, intent, PendingIntent.FLAG_IMMUTABLE)
        }

        val defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        assert(icon != null)
        val builder: NotificationCompat.Builder =
            NotificationCompat.Builder(this, MvvmChatApp.CHANNEL_ID)
                .setSmallIcon(icon!!.toInt())
                .setContentTitle(title)
                .setContentText(body)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))
                .setAutoCancel(true)
                .setSound(defaultSound)
                .setContentIntent(pendingIntent)
        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager?
        var i = 0
        if (j > 0) {
            i = j
        }
        assert(notificationManager != null)
        notificationManager!!.notify(i, builder.build())
    }

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        Log.d("aaa", token)
    }
}