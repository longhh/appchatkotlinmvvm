package mvvm.chat.fcm

class Sender {
    var data: Data? = null
    var to: String? = null

    constructor(data: Data?, to: String?) {
        this.data = data
        this.to = to
    }

    constructor()
}