package mvvm.chat.fcm

class Token {
    var token: String? = null

    constructor()

    constructor(token: String?) {
        this.token = token
    }
}