package mvvm.chat.fcm

class Data {
    var user: String? = null
    var body:String? = null
    var title:String? = null
    var sent:String? = null
    var icon = 0

    constructor()

    constructor(user: String, icon: Int, body: String, title: String, sent: String){
        this.user = user
        this.body = body
        this.title = title
        this.sent = sent
        this.icon = icon
    }

}