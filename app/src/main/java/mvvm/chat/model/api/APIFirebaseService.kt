package mvvm.chat.model.api

import mvvm.chat.fcm.MyResponse
import mvvm.chat.fcm.Sender
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.HeaderMap
import retrofit2.http.Headers
import retrofit2.http.POST

interface APIFirebaseService {
    @Headers(
        "Content-Type:application/json",
        "Authorization:key=AAAAaYQcDVg:APA91bGFuq1BKBBWWczke30kOs4VYJxtDSSLqP518xYI98tvvV4AUhRBLlaJk3K5x-W3loyw4QS6C2b69gN9LEth6ulFUZV7el5R-94hdH9unWNR1jajaDRa_5T5u5iBe8MxmRWX_ITy"
    )
    @POST("fcm/send")
    fun sendNotifications(@Body body: Sender?): Call<MyResponse?>?
}