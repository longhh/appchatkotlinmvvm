package mvvm.chat.model

import com.android.mvvm.chat.model.GitResponse
import mvvm.chat.model.api.ApiClient
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.ktx.Firebase
import mvvm.chat.utils.Constants
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RepoRepository {

    // GET repo list
    fun getRepoList(onResult: (isSuccess: Boolean, response: GitResponse?) -> Unit) {

        ApiClient.instance.getRepo().enqueue(object : Callback<GitResponse> {
            override fun onResponse(call: Call<GitResponse>?, response: Response<GitResponse>?) {
                if (response != null && response.isSuccessful)
                    onResult(true, response.body()!!)
                else
                    onResult(false, null)
            }

            override fun onFailure(call: Call<GitResponse>?, t: Throwable?) {
                onResult(false, null)
            }

        })
    }

    companion object {
        private var INSTANCE: RepoRepository? = null
        fun getInstance() = INSTANCE
                ?: RepoRepository().also {
                    INSTANCE = it
                }
    }

    fun generateRoomKey(): String? {
        val userCurrent = Firebase.auth.currentUser
        userCurrent?.let {
            return FirebaseDatabase.getInstance()
                .getReference(Constants.Reference.REFERENCE_ROOMS)
                .child(it.uid)
                .push()
                .key
        }
        return "1"
    }
}