package mvvm.chat.data

import mvvm.chat.utils.Constants

data class UserSort (
    var header: String? = null,
    var users: Users? = null,
    var isSection: Boolean = false,
    var stateUser: Constants.StateUser? = null
)