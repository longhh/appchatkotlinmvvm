package mvvm.chat.data

import com.google.firebase.database.Exclude
import java.io.Serializable
import java.util.*
import kotlin.collections.HashMap

class Room : Serializable {
    var id: String? = null
    var name: String? = null
    var thumbnail: String? = null
    var receiver: String? = null
    var unreadMessage = 0
    var lastMessage: String? = null
    var timestamp: String? = null
    var searchByName: String? = null
    var calling: String? = null
    var typing: String? = "noOne"

    constructor() {}

    constructor(
        id: String,
        name: String,
        thumbnail: String,
        receiver: String,
        unreadMessage: Int,
        lastMessage: String?,
        timestamp: String,
        searchByName: String,
        calling: String,
        typing: String
    ) {
        this.id = id
        this.name = name
        this.thumbnail = thumbnail
        this.receiver = receiver
        this.unreadMessage = unreadMessage
        this.lastMessage = lastMessage
        this.timestamp = timestamp
        this.searchByName = searchByName
        this.calling = calling
        this.typing = typing
    }

    @Exclude
    fun toMap(): Map<String?, Any?> {
        val result = HashMap<String?, Any?>()
        result["id"] = id
        result["name"] = name
        result["thumbnail"] = thumbnail
        result["unreadMessage"] = unreadMessage
        result["lastMessage"] = lastMessage
        result["receiver"] = receiver
        result["timestamp"] = timestamp
        result["searchByName"] = name!!.toLowerCase(Locale.getDefault())
        result["calling"] = calling
        result["typing"] = typing
        return result
    }
}