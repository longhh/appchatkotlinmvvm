package mvvm.chat.data

data class Message (
    var senderId: String? = null,
    var content: String? = null,
    var time: String? = null
)