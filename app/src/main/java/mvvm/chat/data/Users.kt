package mvvm.chat.data

import java.io.Serializable

class Users :Serializable {
    var id: String? = null
    var username: String? = null
    var imageURL: String? = null
    var search: String? = null
    var phoneNumber: String? = null
    var dateOfBirth: String? = null
    var address: String? = null
    var email: String? = null

    constructor() {}
    constructor(
        id: String,
        username: String,
        imageURL: String,
        search: String,
        phoneNumber: String,
        dateOfBirth: String,
        address: String,
        email: String
    ) {
        this.id = id
        this.username = username
        this.imageURL = imageURL
        this.search = search
        this.phoneNumber = phoneNumber
        this.dateOfBirth = dateOfBirth
        this.address = address
        this.email = email
    }
}