package mvvm.chat.data

data class FriendSort (
    var header: String? = null,
    var users: Users? = null,
    var isSection: Boolean = false
)

