package mvvm

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import com.android.mvvm.R
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.ktx.Firebase
import mvvm.chat.data.Room
import mvvm.chat.ui.incomingvideo.InComingVideoFragment
import mvvm.chat.utils.Constants
import mvvm.chat.utils.ContextWrapperHelper
import java.util.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        defaultLanguage()
        handleCalling()
    }

    private fun defaultLanguage() {
        val sharedPreferences = getSharedPreferences("Settings", Activity.MODE_PRIVATE)

        if (sharedPreferences.getString("MyLang", "en").equals("")) {
            val context: Context =
                ContextWrapperHelper.wrap(this, Locale.getDefault().language.toString())
            resources.updateConfiguration(
                context.resources.configuration,
                context.resources.displayMetrics
            )
        } else {
            val context: Context =
                ContextWrapperHelper.wrap(this, sharedPreferences.getString("MyLang", "en")!!)
            resources.updateConfiguration(
                context.resources.configuration,
                context.resources.displayMetrics
            )
        }
    }

    override fun onSupportNavigateUp() = findNavController(R.id.main_nav_fragment).navigateUp()

    fun hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    private fun handleCalling() {
        val user = Firebase.auth.currentUser
        if (user != null) {
            FirebaseDatabase.getInstance().reference.child(Constants.Reference.REFERENCE_ROOMS)
                .child(user.uid)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        for (dataSnapshot in snapshot.children) {
                            val room: Room? = dataSnapshot.getValue(Room::class.java)
                            if (room != null) {
                                if (room.calling.equals("receiver")) {
                                    supportFragmentManager.beginTransaction()
                                        .add(R.id.main_nav_fragment, InComingVideoFragment())
                                        .commit()
                                }
                            }
                        }
                    }

                    override fun onCancelled(error: DatabaseError) {}
                })
        }
    }

    override fun onResume() {
        super.onResume()
        handleCalling()
    }
}