package mvvm

import android.annotation.SuppressLint
import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.os.Build

class MvvmChatApp : Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this
        createChannelNotification()
    }

    companion object {
        lateinit var instance: MvvmChatApp
        const val CHANNEL_ID = "push_notification_id"
    }

    private fun createChannelNotification() {
        @SuppressLint("WrongConstant") val channel = NotificationChannel(
            CHANNEL_ID, "PushNotification", NotificationManager.IMPORTANCE_MAX
        )
        getSystemService(NotificationManager::class.java).createNotificationChannel(channel)
    }
}